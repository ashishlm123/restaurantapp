package com.emts.restaurantapp;

import android.app.Application;

/**
 * Created by User on 2017-04-07.
 */

public class ResApp extends Application {
    static ResApp mInstance;

    @Override
    public void onCreate() {
        super.onCreate();

        mInstance = this;

    }

    public static synchronized ResApp getInstance() {
        return mInstance;
    }
}
