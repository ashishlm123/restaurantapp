package com.emts.restaurantapp;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.core.view.GravityCompat;
import androidx.core.view.MenuItemCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.restaurantapp.activity.CartActivity;
import com.emts.restaurantapp.activity.LoginActivity;
import com.emts.restaurantapp.activity.RegisterActivity;
import com.emts.restaurantapp.fragments.AboutFragment;
import com.emts.restaurantapp.fragments.CategoryFrag;
import com.emts.restaurantapp.fragments.ContactUsFrag;
import com.emts.restaurantapp.fragments.HomeFragment;
import com.emts.restaurantapp.fragments.OrderHistoryFrag;
import com.emts.restaurantapp.fragments.SearchFrag;
import com.emts.restaurantapp.helper.Api;
import com.emts.restaurantapp.helper.NetworkUtils;
import com.emts.restaurantapp.helper.PreferenceHelper;
import com.emts.restaurantapp.helper.VolleyHelper;
import com.emts.restaurantapp.models.CategoryModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    DrawerLayout drawer;
    FragmentManager fm;
    PreferenceHelper prefsHelper;

    TextView cartCount;
    NavigationView navigationView;
    View views;
    MenuItem mPreviousMenuItem;
    Toolbar toolbar;
    TextView user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        fm = getSupportFragmentManager();
        prefsHelper = PreferenceHelper.getInstance(getApplicationContext());

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.inflateMenu(R.menu.main);

        Menu menu = toolbar.getMenu();
        MenuItem menu_comments = menu.findItem(R.id.action_cart);
        MenuItemCompat.setActionView(menu_comments, R.layout.badge_layout);
        View v = MenuItemCompat.getActionView(menu_comments);
        cartCount = v.findViewById(R.id.counter);

        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (prefsHelper.isLogin()) {
                    Intent intent = new Intent(getApplicationContext(), CartActivity.class);
                    startActivity(intent);
                } else {
                    Snackbar.make(findViewById(android.R.id.content), "Please, Login To View Cart...", Snackbar.LENGTH_LONG)
                            .setActionTextColor(Color.RED)
                            .show();
                }
            }
        });

        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                // Handle navigation view item clicks here.
                item.setCheckable(true);
                item.setChecked(true);
                if (mPreviousMenuItem != null) {
                    mPreviousMenuItem.setChecked(false);
                }
                mPreviousMenuItem = item;

                int id = item.getItemId();
                Fragment fragment = null;
                Bundle bundle = new Bundle();
                switch (id) {
                    case R.id.action_cart:
                        if (prefsHelper.isLogin()) {
                            Intent intent = new Intent(getApplicationContext(), CartActivity.class);
                            startActivity(intent);
                        } else {
                            Snackbar.make(findViewById(android.R.id.content), "Please, Login To View Cart...", Snackbar.LENGTH_LONG)
                                    .setActionTextColor(Color.RED)
                                    .show();
                        }
                        break;
                    case R.id.action_search:
                        fragment = new SearchFrag();
                        break;
                }

                if (fragment != null) {
                    fm.beginTransaction().replace(R.id.content_main, fragment)
                            .addToBackStack(null)
                            .commit();
                    drawer.closeDrawer(GravityCompat.START);
                }
                setTitle();

                return false;
            }
        });

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        if (prefsHelper.isLogin()) {
            navigationView.inflateMenu(R.menu.activity_main_drawer_logged);
        } else {
            navigationView.inflateMenu(R.menu.activity_main_drawer);
        }

        views = navigationView.getHeaderView(0);

        navigationView.setNavigationItemSelectedListener(this);

        setHeaderData();

        //default home fragment
        fm.beginTransaction().add(R.id.content_main, new HomeFragment(), "Home").commit();
        setTitle();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (NetworkUtils.isInNetwork(getApplicationContext())) {
            userCartCountTask();
        }
    }

    public void setHeaderData() {
        LinearLayout loginView = views.findViewById(R.id.holder_logged_in);
        RelativeLayout logoutView = views.findViewById(R.id.holder_logged_out);

        if (prefsHelper.isLogin()) {
            loginView.setVisibility(View.VISIBLE);
            user = views.findViewById(R.id.user);
            user.setText(prefsHelper.getUserName());
            if (prefsHelper.getUserName().equals("")) {
                user.setText(prefsHelper.getUserEmail());
            }
            logoutView.setVisibility(View.GONE);
        } else {
            logoutView.setVisibility(View.VISIBLE);
            loginView.setVisibility(View.GONE);

            Button btnLogin = views.findViewById(R.id.btn_login);
            Button btnRegister = views.findViewById(R.id.btn_register);
            btnLogin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(MainActivity.this, LoginActivity.class));
                }
            });
            btnRegister.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(MainActivity.this, RegisterActivity.class));
                }
            });
        }
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        item.setCheckable(true);
        item.setChecked(true);
        if (mPreviousMenuItem != null) {
            mPreviousMenuItem.setChecked(false);
        }
        mPreviousMenuItem = item;

        int id = item.getItemId();
        Fragment fragment = null;
        Bundle bundle = new Bundle();
        switch (id) {
            case R.id.nav_home:
                fragment = new HomeFragment();
                break;
            case R.id.nav_contact_us:
                fragment = new ContactUsFrag();
                break;
            case R.id.nav_about_us:
                fragment = new AboutFragment();
                break;
            case R.id.nav_search:
                fragment = new SearchFrag();
                break;
            case R.id.nav_category:
                fragment = new CategoryFrag();
                break;
            case R.id.nav_cart:
                Intent intent = new Intent(getApplicationContext(), CartActivity.class);
                startActivity(intent);
                break;
            case R.id.nav_order_history:
                fragment = new OrderHistoryFrag();
                break;
            case R.id.nav_logout:
                prefsHelper.edit().clear().commit();
                Intent intent1 = new Intent(getApplicationContext(), MainActivity.class);
                intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK |
                        Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent1);
                break;

        }

        if (fragment != null) {
            fm.beginTransaction().replace(R.id.content_main, fragment)
                    .addToBackStack(null)
                    .commit();
            drawer.closeDrawer(GravityCompat.START);
        }
        setTitle();

        return true;
    }

    public void setTitle() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Fragment fragment = fm.findFragmentById(R.id.content_main);
                if (fragment instanceof HomeFragment) {
                    toolbar.setTitle("Home");
                } else if (fragment instanceof ContactUsFrag) {
                    toolbar.setTitle("Contact Us");
                } else if (fragment instanceof AboutFragment) {
                    toolbar.setTitle("About");
                } else if (fragment instanceof SearchFrag) {
                    toolbar.setTitle("Search");
                } else if (fragment instanceof CategoryFrag) {
                    toolbar.setTitle("Category");
                } else if (fragment instanceof OrderHistoryFrag) {
                    toolbar.setTitle("Order History");
                } else {
                    toolbar.setTitle(getString(R.string.app_name));
                }
            }
        }, 200);
    }


    public void openSearchFragment(CategoryModel categoryModel) {
        Fragment fragment = new SearchFrag();
        Bundle bundle = new Bundle();
        if (categoryModel != null) {
            bundle.putSerializable("Category", categoryModel);
            fragment.setArguments(bundle);
        }
        fm.beginTransaction().replace(R.id.content_main, fragment).addToBackStack(null).commit();
        setTitle();
    }

    private void userCartCountTask() {
        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postMap = vHelper.getPostParams();

        postMap.put("user_id", prefsHelper.getUserId());

        vHelper.addVolleyRequestListeners(Api.getInstance().cartCountUrl, Request.Method.POST, postMap, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                try {
                    JSONObject res = new JSONObject(response);
                    if (res.getBoolean("status")) {
                        int count = res.getInt("cart_count");
                        if (count > 0) {
                            cartCount.setText(String.valueOf(count));
                            cartCount.setVisibility(View.VISIBLE);
                        }
                    } else {

                    }

                } catch (JSONException e) {

                }
            }

            @Override
            public void onError(String errorResponse, VolleyError volleyError) {

            }
        }, "userCartCountTask");
    }


}
