package com.emts.restaurantapp.adapter;

import android.content.Context;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.emts.restaurantapp.MainActivity;
import com.emts.restaurantapp.R;
import com.emts.restaurantapp.models.CategoryModel;

import java.util.ArrayList;

/**
 * Created by Ashish on 25/07/2017.
 */

public class VerticalCategoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<CategoryModel> categoryList;

    public VerticalCategoryAdapter(ArrayList<CategoryModel> cuisinesList, Context context) {
        this.categoryList = cuisinesList;
        this.context = context;
    }



    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_vertical_cuisines, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ViewHolder holder1= (ViewHolder)holder;
        CategoryModel categoryModel = categoryList.get(position);
        holder1.textView.setText(categoryModel.getCuisineName());
    }

    @Override
    public int getItemCount() {
        return categoryList.size();
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        TextView textView;


        ViewHolder(View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.cuisines_name);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((MainActivity)context).openSearchFragment(categoryList.get(getLayoutPosition()));
                }
            });


        }
    }
}
