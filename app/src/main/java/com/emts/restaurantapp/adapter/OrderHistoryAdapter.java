package com.emts.restaurantapp.adapter;

import android.content.Context;

import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.emts.restaurantapp.R;
import com.emts.restaurantapp.models.OrderHistoryModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Prabin on 8/11/2017.
 */

public class OrderHistoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<OrderHistoryModel> orderList;

    public OrderHistoryAdapter(Context context, ArrayList<OrderHistoryModel> orderList) {
        this.orderList = orderList;
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_order_history, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        OrderHistoryAdapter.ViewHolder viewHolder = (OrderHistoryAdapter.ViewHolder) holder;
        OrderHistoryModel orderHistoryModel = orderList.get(position);
        viewHolder.date.setText(orderHistoryModel.getDateTime());
        viewHolder.status.setText(orderHistoryModel.getStatus());
        viewHolder.invoiceId.setText(orderHistoryModel.getInvoice_id());
        viewHolder.city.setText(orderHistoryModel.getCity());
        viewHolder.address.setText(orderHistoryModel.getAddress());
        viewHolder.total.setText("$ " + String.format("%.2f", orderHistoryModel.getTotalPrice()));


    }

    @Override
    public int getItemCount() {
       return orderList.size();
    }

    private class ViewHolder extends RecyclerView.ViewHolder {

        TextView date, status, invoiceId, city, address, total;

        public ViewHolder(View itemView) {
            super(itemView);
            date= itemView.findViewById(R.id.datetime);
            status= itemView.findViewById(R.id.status);
            invoiceId = itemView.findViewById(R.id.invoice_id);
            city = itemView.findViewById(R.id.city);
            address = itemView.findViewById(R.id.address);
            total = itemView.findViewById(R.id.total_price);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });
        }
    }
}
