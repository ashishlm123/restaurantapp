package com.emts.restaurantapp.adapter;

import android.content.Context;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.emts.restaurantapp.MainActivity;
import com.emts.restaurantapp.R;
import com.emts.restaurantapp.models.CategoryModel;

import java.util.ArrayList;

/**
 * Created by Ashish on 25/07/2017.
 */
public class CategoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<CategoryModel> categoryList;
    int[] colorArray = new int[]{R.color.c_blue, R.color.c_purple, R.color.c_green, R.color.c_blue_dark,
            R.color.c_red, R.color.c_green_dark, R.color.c_yelloo_dark, R.color.c_teal_dark, R.color.c_red_dark, R.color.c_purple,
            R.color.c_purple, R.color.c_green, R.color.c_blue_dark,
            R.color.c_red, R.color.c_green_dark, R.color.c_yelloo_dark, R.color.c_teal_dark, R.color.c_red_dark, R.color.c_purple,
            R.color.c_purple, R.color.c_green, R.color.c_blue_dark,
            R.color.c_red, R.color.c_green_dark, R.color.c_yelloo_dark, R.color.c_teal_dark, R.color.c_red_dark, R.color.c_purple,
            R.color.c_purple, R.color.c_green, R.color.c_blue_dark,
            R.color.c_red, R.color.c_green_dark, R.color.c_yelloo_dark, R.color.c_teal_dark, R.color.c_red_dark, R.color.c_purple,
            R.color.c_purple, R.color.c_green, R.color.c_blue_dark,
            R.color.c_red, R.color.c_green_dark, R.color.c_yelloo_dark, R.color.c_teal_dark, R.color.c_red_dark, R.color.c_purple};

    public CategoryAdapter(ArrayList<CategoryModel> categoryList, Context context) {
        this.categoryList = categoryList;
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CategoryAdapter.ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_cuisines_list, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ViewHolder viewHolder = (ViewHolder) holder;
        CategoryModel categoryModel = categoryList.get(position);

        viewHolder.rootCard.setCardBackgroundColor(context.getResources().getColor(colorArray[position % (categoryList.size() - 1)]));
        viewHolder.catName.setText(categoryList.get(position).getCuisineName());
    }

    @Override
    public int getItemCount() {
        return categoryList.size();
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        CardView rootCard;
        TextView catName;

        ViewHolder(View itemView) {
            super(itemView);

            rootCard = (CardView) itemView;
            catName = itemView.findViewById(R.id.cuisine_name);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((MainActivity)context).openSearchFragment(categoryList.get(getLayoutPosition()));
                }
            });
        }
    }
}