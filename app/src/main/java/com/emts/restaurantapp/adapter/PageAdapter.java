package com.emts.restaurantapp.adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.emts.restaurantapp.fragments.MenuTodayFragment;
import com.emts.restaurantapp.fragments.MostSellingFragment;


/**
 * Created by Prabin on 8/3/2017.
 */

public class PageAdapter extends FragmentStatePagerAdapter {

    int mNumOfTabs;

    public PageAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                MostSellingFragment tab1 = new MostSellingFragment();
                return tab1;
            case 1:
                MenuTodayFragment tab2 = new MenuTodayFragment();
                return tab2;


            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}

