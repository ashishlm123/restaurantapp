package com.emts.restaurantapp.adapter;

import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.emts.restaurantapp.R;
import com.emts.restaurantapp.activity.CartActivity;
import com.emts.restaurantapp.activity.FoodDetailsActivity;
import com.emts.restaurantapp.models.FoodMdl;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Ashish on 25/07/2017.
 */
public class FoodAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<FoodMdl> foodList;

    public FoodAdapter(Context context, ArrayList<FoodMdl> foodList) {
        this.foodList = foodList;
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_food, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ViewHolder viewHolder = (ViewHolder) holder;
        FoodMdl foodMdl = foodList.get(position);
        viewHolder.foodName.setText(foodMdl.getFoodName());
        viewHolder.foodPrice.setText("$ " + String.format("%.2f", foodMdl.getFoodPrice()));
        if (!TextUtils.isEmpty(foodMdl.getFoodImage())) {
            Picasso.with(context).load(foodMdl.getFoodImage()).into(viewHolder.foodImage);
        }
        viewHolder.ingredients.setText(foodMdl.getIngredients());
    }


    @Override
    public int getItemCount() {
        return foodList.size();
    }

    private class ViewHolder extends RecyclerView.ViewHolder {

        ImageView cart;
        TextView foodPrice, foodName, ingredients;
        ImageView foodImage;

        ViewHolder(View itemView) {
            super(itemView);

            foodPrice = itemView.findViewById(R.id.food_price);
            foodName = itemView.findViewById(R.id.food_name);
            foodImage = itemView.findViewById(R.id.food_icon);
            ingredients = itemView.findViewById(R.id.ingredients);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, FoodDetailsActivity.class);
                    intent.putExtra("item", foodList.get(getLayoutPosition()));
                    context.startActivity(intent);
                }
            });

            cart = itemView.findViewById(R.id.cart);
            cart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, CartActivity.class);
                    context.startActivity(intent);
                }
            });
        }
    }
}