package com.emts.restaurantapp.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.emts.restaurantapp.R;
import com.emts.restaurantapp.models.CartModel;

import java.util.ArrayList;

/**
 * Created by Ashish on 28/07/2017.
 */

public class CheckOutAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private ArrayList<CartModel> checkOutList;


    public CheckOutAdapter(Context context, ArrayList<CartModel> checkOutList) {
        this.checkOutList = checkOutList;
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_my_order_list_view, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ViewHolder viewHolder = (ViewHolder) holder;
        CartModel cartModel = checkOutList.get(position);
        viewHolder.itemName.setText(cartModel.getFoodName());
        viewHolder.qty.setText(String.valueOf(cartModel.getQty()));
        viewHolder.totalPrice.setText("$ " + String.valueOf(cartModel.getTotalPrice()));

    }

    @Override
    public int getItemCount() {
        return checkOutList.size();
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        TextView qty, itemName, totalPrice;

        ViewHolder(View itemView) {
            super(itemView);
            qty = itemView.findViewById(R.id.qty);
            itemName = itemView.findViewById(R.id.food_name);
            totalPrice = itemView.findViewById(R.id.food_price);
        }
    }
}
