package com.emts.restaurantapp.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.emts.restaurantapp.R;
import com.emts.restaurantapp.activity.CartActivity;
import com.emts.restaurantapp.helper.AlertUtils;
import com.emts.restaurantapp.helper.Api;
import com.emts.restaurantapp.helper.Logger;
import com.emts.restaurantapp.helper.NetworkUtils;
import com.emts.restaurantapp.helper.VolleyHelper;
import com.emts.restaurantapp.models.CartModel;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Ashish on 26/07/2017.
 */

public class CartAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<CartModel> cartList;

    public CartAdapter(Context context, ArrayList<CartModel> cartList) {
        this.cartList = cartList;
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_cart_view, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ViewHolder viewHolder = (ViewHolder) holder;
        CartModel cartModel = cartList.get(position);
        viewHolder.foodName.setText(cartModel.getFoodName());
        viewHolder.foodQuantity.setText(String.valueOf(cartModel.getQty()));
        viewHolder.foodPrice.setText("$ " + String.format("%.2f", cartModel.getFoodPrice()));
        viewHolder.totalPrice.setText("$ " + String.format("%.2f", cartModel.getTotalPrice()));
        if (!TextUtils.isEmpty(cartModel.getFoodImage())) {
            Picasso.with(context).load(cartModel.getFoodImage()).into(viewHolder.foodIcon);
        }
    }

    @Override
    public int getItemCount() {
        return cartList.size();
    }

    private class ViewHolder extends RecyclerView.ViewHolder {

        TextView foodName, foodQuantity, foodPrice, totalPrice, minus, plus;
        ImageView foodIcon, cross;


        ViewHolder(final View itemView) {
            super(itemView);
            foodName = itemView.findViewById(R.id.food_name);
            foodQuantity = itemView.findViewById(R.id.qty);
            foodPrice = itemView.findViewById(R.id.food_price);
            totalPrice = itemView.findViewById(R.id.total_price);
            foodIcon = itemView.findViewById(R.id.food_icon);

            minus = itemView.findViewById(R.id.minus);
            minus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    CartModel cartModel = cartList.get(getLayoutPosition());
                    if (cartModel.getQty() > 1) {
                        if (NetworkUtils.isInNetwork(context)) {
                            cartModel.setQty(cartModel.getQty() - 1);
                            double totalPrice = cartModel.getFoodPrice() * cartModel.getQty();
                            cartModel.setTotalPrice(totalPrice);
                            notifyItemChanged(getLayoutPosition());
                            ((CartActivity) context).updateWholePrice();
                            userQuantityUpdateTask(getLayoutPosition(), cartModel.getQty(), view);

                        } else {
                            AlertUtils.showSnack(context, view, "No internet connection...");
                        }
                    }

                }
            });

            plus = itemView.findViewById(R.id.plus);
            plus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    CartModel cartModel = cartList.get(getLayoutPosition());

                    if (NetworkUtils.isInNetwork(context)) {
                        cartModel.setQty(cartModel.getQty() + 1);
                        double totalPrice = cartModel.getFoodPrice() * cartModel.getQty();
                        cartModel.setTotalPrice(totalPrice);
                        notifyItemChanged(getLayoutPosition());
                        ((CartActivity) context).updateWholePrice();
                        userQuantityUpdateTask(getLayoutPosition(), cartModel.getQty(), view);

                    } else {
                        AlertUtils.showSnack(context, view, "No internet connection...");
                    }

                }
            });

            cross = itemView.findViewById(R.id.delete_cart);
            cross.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (NetworkUtils.isInNetwork(context)) {
                        userCartDeleteTask(cartList.get((getLayoutPosition())), getLayoutPosition(), view);

                    } else {
                        AlertUtils.showSnack(context, view, "No internet connection...");
                    }
                }
            });


        }
    }


    private void userCartDeleteTask(final CartModel cartModel, final int pos, final View view) {
        final ProgressDialog dialog = AlertUtils.showProgressDialog(context, "Please Wait...");

        VolleyHelper vHelper = VolleyHelper.getInstance(context);
        HashMap<String, String> postMap = vHelper.getPostParams();
        postMap.put("id", cartModel.getCartId());

        vHelper.addVolleyRequestListeners(Api.getInstance().deleteCartUrl, Request.Method.POST, postMap,
                new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        dialog.dismiss();
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                cartList.remove(pos);
                                AlertUtils.showSnack(context, view, res.getString("message"));
                                notifyItemRemoved(pos);
                                ((CartActivity) context).updateWholePrice();

                                if (cartList.size() == 0) {
                                    ((CartActivity) context).cartMessage();
                                }

                            } else {
                                dialog.dismiss();
                                AlertUtils.showSnack(context, view, res.getString("message"));
                            }

                        } catch (JSONException e) {
                            Logger.e("userCartDeleteTask json ex", e.getMessage() + "");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError error) {
                        dialog.dismiss();
                        try {
                            JSONObject errorObj = new JSONObject(new String(error.networkResponse.data));
                            AlertUtils.showSnack(context, view, errorObj.getString("message"));
                        } catch (Exception e) {
                            if (error instanceof NetworkError) {
                                Toast.makeText(context, "No Internet Connection...", Toast.LENGTH_SHORT).show();
                            } else if (error instanceof ServerError) {
                                Toast.makeText(context, "Server Error...", Toast.LENGTH_SHORT).show();
                            } else if (error instanceof AuthFailureError) {
                                Toast.makeText(context, "Authentication Error...", Toast.LENGTH_SHORT).show();
                            } else if (error instanceof ParseError) {
                                Toast.makeText(context, "Parse Error", Toast.LENGTH_SHORT).show();
                            } else if (error instanceof TimeoutError) {
                                Toast.makeText(context, "Timed Out", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }, "userCartDeleteTask");

    }

    private void userQuantityUpdateTask(final int pos, final int qty, final View view) {
        VolleyHelper vHelper = VolleyHelper.getInstance(context);
        HashMap<String, String> postMap = vHelper.getPostParams();
        postMap.put("quantity", String.valueOf(qty));

        vHelper.addVolleyRequestListeners(Api.getInstance().itemQuantityUpdateUrl, Request.Method.POST, postMap,
                new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {

                            } else {
                                AlertUtils.showSnack(context, view, res.getString("message"));
                            }
                        } catch (JSONException e) {
                            Logger.e("userQuantityUpdateTask json ex", e.getMessage() + "");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError error) {
                        try {
                            JSONObject errorObj = new JSONObject(new String(error.networkResponse.data));
                            AlertUtils.showSnack(context, view, errorObj.getString("message"));
                        } catch (Exception e) {
                            if (error instanceof NetworkError) {
                                Toast.makeText(context, "No Internet Connection...", Toast.LENGTH_SHORT).show();
                            } else if (error instanceof ServerError) {
                                Toast.makeText(context, "Server Error...", Toast.LENGTH_SHORT).show();
                            } else if (error instanceof AuthFailureError) {
                                Toast.makeText(context, "Authentication Error...", Toast.LENGTH_SHORT).show();
                            } else if (error instanceof ParseError) {
                                Toast.makeText(context, "Parse Error", Toast.LENGTH_SHORT).show();
                            } else if (error instanceof TimeoutError) {
                                Toast.makeText(context, "Timed Out", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }, "userQuantityUpdateTask");


    }

}
