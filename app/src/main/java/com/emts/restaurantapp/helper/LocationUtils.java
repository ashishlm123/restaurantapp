package com.emts.restaurantapp.helper;

import android.location.Location;

/**
 * Created by theone on 10/30/2016.
 */

public class LocationUtils {

    public static float locationDifferenceMeter(Location l1, Location l2) {
        return l1.distanceTo(l2);
    }

    public static float locationDifferenceMiles(Location loc1, Location loc2) {
//        Location loc1 = new Location("");
//        loc1.setLatitude(lat1);
//        loc1.setLongitude(lon1);
//
//        Location loc2 = new Location("");
//        loc2.setLatitude(lat2);
//        loc2.setLongitude(lon2);

        return (float) ((loc1.distanceTo(loc2) * 0.000621371192));//returns distance in meter
    }

//    function getMiles(i) {
//        return i*0.000621371192;
//    }
//    function getMeters(i) {
//        return i*1609.344;
//    }

    public static float locationDifferenceKm(Location loc1, Location loc2) {
//        Location loc1 = new Location("");
//        loc1.setLatitude(lat1);
//        loc1.setLongitude(lon1);
//
//        Location loc2 = new Location("");
//        loc2.setLatitude(lat2);
//        loc2.setLongitude(lon2);

        return ((int) loc1.distanceTo(loc2) / 1000);//returns distance in meter
    }

    // 1 mile = 1.60934 Km
    public static int convertMilesToKm(int miles) {
        return (int) Math.ceil(miles * 1.60934);
    }
}
