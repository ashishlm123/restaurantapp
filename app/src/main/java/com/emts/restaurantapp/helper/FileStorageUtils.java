package com.emts.restaurantapp.helper;

import android.content.Context;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by User on 2017-04-07.
 */

public class FileStorageUtils {

    public static void writeToCache(Context context, String fName, String fContent) {
        File file = new File(context.getCacheDir(), fName);
        FileWriter fw = null;
        try {
            fw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(fContent);
            bw.close();
        } catch (IOException e) {
            Logger.e("cache write ex", e.getMessage() + " ");
        }
    }

    public static String readFromCache(Context context, String fName) {
        try {
            StringBuilder fData = new StringBuilder("");
            FileInputStream fis = new FileInputStream(new File(context.getCacheDir(), fName));
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader buffreader = new BufferedReader(isr);

            String readString = buffreader.readLine();
            while (readString != null) {
                fData.append(readString);
                readString = buffreader.readLine();
            }

            isr.close();
            return fData.toString();
        } catch (FileNotFoundException e) {
            Logger.e("cache read ex", e.getMessage() + " ");
        } catch (IOException e) {
            Logger.e("cache read ex 2", e.getMessage() + " ");
        }
        return "";
    }
}
