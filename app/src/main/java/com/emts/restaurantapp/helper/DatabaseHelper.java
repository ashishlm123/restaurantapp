package com.emts.restaurantapp.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by theone on 3/23/2016.
 */
public class DatabaseHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "chasebid.db";
    private static final int DATABASE_VERSION = 1;
    private static DatabaseHelper instance = null;

    private final String TAG = "Database";

    //table cat and subcat concat
    private static final String TABLE_CAT = "table_categories";
    private static final String ID = "_id";
    private static final String CAT_ID = "cat_id";
    private static final String CAT_NAME = "name";
    private static final String PARENT_ID = "parent_id";
    private static final String LAST_UPDATE_DATE = "last_update";
    private static final String IMAGE_URL = "image_url";
    private static final String IMAGE_URL_SMALL="image_url_small";
    private static final String CAT_ICON = "icon";

    private String CREATE_TABLE_CATEGORY = "create table " + TABLE_CAT + "(" + ID + " integer primary key autoincrement, "
            + CAT_ID + " integer, " + PARENT_ID + " integer, " + CAT_NAME + " text, " + LAST_UPDATE_DATE + " text, " + IMAGE_URL
            + " text, "+ IMAGE_URL_SMALL + " text, " + CAT_ICON + " blob)";

    public static DatabaseHelper getInstance(Context context) {
        if (instance == null) {
            instance = new DatabaseHelper(context.getApplicationContext());
        }
        return instance;
    }

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_CATEGORY);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CAT);
    }

    public long categoryCount() {
        SQLiteDatabase db = getReadableDatabase();
        long count = DatabaseUtils.queryNumEntries(db, TABLE_CAT);
        db.close();
        return count;
    }
//
//    public void insertCategories(ArrayList<CategoryItem> categoryItems) {
//        SQLiteDatabase db = this.getWritableDatabase();
//        ContentValues values = new ContentValues();
//        Logger.e("categories to save", categoryItems.size() + " '");
//        for (int i = 0; i < categoryItems.size(); i++) {
//            CategoryItem catItem = categoryItems.get(i);
//            if (catItem.getSubCategoryId() > 0) {
//                values.put(CAT_ID, catItem.getSubCategoryId());
//                values.put(PARENT_ID, catItem.getCategoryId());
//            } else {
//                values.put(CAT_ID, catItem.getCategoryId());
//                values.put(PARENT_ID, -1);
//            }
//            values.put(CAT_NAME, catItem.getTitle());
//            values.put(IMAGE_URL, catItem.getImageUrl());
//            values.put(IMAGE_URL_SMALL,catItem.getImageUrlSmall());
////            values.put(CAT_ICON_BLOB, "");
//            values.put(LAST_UPDATE_DATE, catItem.getLastUpdated());
//
//            db.insert(TABLE_CAT, null, values);
//        }
//    }
//
//    public String getLastUpdateDate() {
//        SQLiteDatabase db = this.getReadableDatabase();
//        String query = "select " + LAST_UPDATE_DATE + " from " + TABLE_CAT + " order by " + LAST_UPDATE_DATE + " desc limit 1";
//        try (Cursor c = db.rawQuery(query, null)) {
//            if (c != null) {
//                if (c.getCount() > 0 && c.moveToFirst()) {
//                    return c.getString(0);
//                }
//            }
//        } catch (Exception e) {
//            Logger.e("fetchLastUdate ex", e.getMessage());
//        }
//        return null;
//    }
//
//    public ArrayList<CategoryItem> getAllCategory() {
//        ArrayList<CategoryItem> allCat = new ArrayList<>();
//
//        SQLiteDatabase db = getReadableDatabase();
//        String query = "select " + CAT_ID + ", " + CAT_NAME + ", "+ IMAGE_URL + ", " +IMAGE_URL_SMALL+
//                " from " + TABLE_CAT + " where "
//                + PARENT_ID + "='-1'";
//        Cursor cus = db.rawQuery(query, null);
//        if (cus != null && cus.getCount() > 0) {
//            if (cus.moveToFirst()) {
//                do {
//                    CategoryItem catItem = new CategoryItem();
//                    catItem.setCategoryId(cus.getInt(0));
//                    catItem.setTitle(cus.getString(cus.getColumnIndex(CAT_NAME)));
//                    catItem.setImageUrl(cus.getString(cus.getColumnIndex(IMAGE_URL)));
//                    catItem.setImageUrlSmall(cus.getString(cus.getColumnIndex(IMAGE_URL_SMALL)));
//
//                    allCat.add(catItem);
//                } while (cus.moveToNext());
//            }
//        }
//        if (cus != null && !cus.isClosed()) {
//            cus.close();
//        }
//        db.close();
//
//        return allCat;
//    }
//
//    public List<CategoryItem> getSubCatByCatId(int categoryId) {
//        SQLiteDatabase db = getReadableDatabase();
//        List<CategoryItem> subCatList = new ArrayList<>();
//
//        String[] tableColumns = new String[]{
//                CAT_ID,
//                CAT_NAME
//        };
//        String whereClause = PARENT_ID + " = ?";
//        String[] whereArgs = new String[]{
//                String.valueOf(categoryId)
//        };
//        try (Cursor c = db.query(TABLE_CAT, tableColumns, whereClause, whereArgs,
//                null, null, null)) {
//            if (c != null && c.getCount() > 0) {
//                if (c.moveToFirst()) {
//                    do {
//                        CategoryItem subCatItem = new CategoryItem();
//                        subCatItem.setCategoryId(categoryId);
//                        subCatItem.setSubCategoryId(c.getInt(c.getColumnIndex(CAT_ID)));
//                        subCatItem.setTitle(c.getString(c.getColumnIndex(CAT_NAME)));
//
//                        subCatList.add(subCatItem);
//                    } while (c.moveToNext());
//                }
//            }
//        } catch (Exception e) {
//            Logger.e(TAG + " getSubCatByCatId ex", e.getMessage() + "");
//        }
//        return subCatList;
//    }

    public void clearCategoryTable() {
        SQLiteDatabase db = getWritableDatabase();
        try {
            db.delete(TABLE_CAT, null, null);
        } catch (Exception e) {
            Logger.e(TAG + " clearCategoryTable ex", e.getMessage() + "");
        }
        db.close();
    }

}
