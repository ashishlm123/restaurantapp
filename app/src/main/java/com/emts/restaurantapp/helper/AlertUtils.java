package com.emts.restaurantapp.helper;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import android.text.Html;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.emts.restaurantapp.R;

/**
 * Created by User on 2016-10-19.
 */

public class AlertUtils {


    public static interface OnAlertButtonClickListener {
        void onAlertButtonClick(boolean isPositiveButton);
    }

    public static void showAlertMessage(Context context, String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        if (!TextUtils.isEmpty(title)) {
            builder.setTitle(title);
        }
        builder.setMessage(Html.fromHtml(message));
        builder.show();
    }

    public static void showSnack(Context context, View view, String message) {
        Snackbar snackbar = Snackbar.make(view, message, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        TextView tv = (TextView) snackBarView.findViewById(android.support.design.R.id.snackbar_text);
        tv.setTextColor(Color.WHITE);
        tv.setGravity(Gravity.CENTER);
        snackBarView.setBackgroundColor(ContextCompat.getColor(context,
                R.color.colorAccent));
        snackbar.show();
    }

    public static void showToast(Context context, String message) {
        try {
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Logger.e("alertUtils showToast ex", e.getMessage() + "");
        }
    }

    public static void showErrorAlert(Context context, String message, final OnAlertButtonClickListener listener) {
        final AlertDialog.Builder alertDailog = new AlertDialog.Builder(context);
        final View view = LayoutInflater.from(context).inflate(R.layout.alert_message, null);
        alertDailog.setView(view);
        final TextView alert_message = view.findViewById(R.id.alert_message);
        alert_message.setText(message);
        final Dialog dialog = alertDailog.show();

        final Button ok = view.findViewById(R.id.btn_ok);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                if (listener!= null){
                    listener.onAlertButtonClick(true);
                }
            }
        });

    }

    public static ProgressDialog showProgressDialog(Context context, String message) {
        ProgressDialog pDialog = new ProgressDialog(context);
        pDialog.setCanceledOnTouchOutside(false);
        pDialog.setMessage(message);
        pDialog.show();
        return pDialog;
    }

    public static void hideInputMethod(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

    }
}
