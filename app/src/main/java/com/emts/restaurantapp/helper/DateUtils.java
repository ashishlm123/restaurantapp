package com.emts.restaurantapp.helper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by User on 2016-10-19.
 */

public class DateUtils {

    //convert form dd/MM/YYYY HH:mm to dd MMM YYYY, hh:mm a
    public static String convertDate(String dateToConvert) {
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm");
        Date testDate = null;
        try {
            testDate = sdf.parse(dateToConvert);
        } catch (Exception ex) {
            Logger.e("DateUtils ex 12", ex.getMessage() + "");
            return "";
        }
        SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy, hh:mm a");
        return formatter.format(testDate);
    }

    //convert form dd/MM/YYYY to dd MMM YYYY
    public static String convertDateWithoutTime(String dateToConvert) {
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        Date testDate = null;
        try {
            testDate = sdf.parse(dateToConvert);
        } catch (Exception ex) {
            Logger.e("DateUtils ex 12", ex.getMessage() + "");
            return "";
        }
        SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy");
        return formatter.format(testDate);
    }

    public static String convertTime(String _24HourTime) {
        try {
            SimpleDateFormat _24HourSDF = new SimpleDateFormat("HH:mm");
            SimpleDateFormat _12HourSDF = new SimpleDateFormat("hh:mm a");
            Date _24HourDt = _24HourSDF.parse(_24HourTime);
            return _12HourSDF.format(_24HourDt);
        } catch (Exception e) {
            return "";
        }
    }

    public static String getPostTimeAgo(String createdDate) {
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone("UTF"));
        try {
            Date dateCreated = sdf.parse(createdDate);
            Date dateNow = new Date();
            dateNow.setTime(System.currentTimeMillis());
//            Logger.e("created VS current time", dateCreated.getTime() + " VS " + dateNow.getTime());

            long difference = dateNow.getTime() - dateCreated.getTime();
//            Logger.e("difference", difference + "");
//            int days = (int) (difference / (1000*60*60*24));
//            int hours = (int) ((difference - (1000*60*60*24*days)) / (1000*60*60));
//            int min = (int) (difference - (1000*60*60*24*days) - (1000*60*60*hours)) / (1000*60);
//            Logger.e("dd, hh, mm", days + " : " + hours + " : " + min);
//            hours = (hours < 0 ? -hours : hours);

//            if (days > 0){
//                return days + " days ago";
//            }else if (hours > 0){
//                return hours + " hours ago";
//            }else if (min > 0){
//                return min + " mins ago";
//            }else{
//                return "now";
//            }


            long seconds = difference / 1000;
            long minutes = seconds / 60;
            long hours = minutes / 60;
            long days = hours / 24;

            if (dateCreated.before(dateNow)) {

//                Logger.e("oldDate", "is previous date");
//                Logger.e("Difference: ", " seconds: " + seconds + " minutes: " + minutes
//                        + " hours: " + hours + " days: " + days);
            }

            if (days > 0) {
                return days + " days ago";
            } else if (hours > 0) {
                return hours + " hours ago";
            } else if (minutes > 0) {
                return minutes + " mins ago";
            } else if (seconds > 0) {
                return seconds + "sec ago";
            } else {
                return "now";
            }


        } catch (ParseException ex) {
            Logger.e("date diff pares ex", ex.getMessage() + "");
            return "now";
        } catch (NullPointerException e) {
            Logger.e("date diff ex 11", e.getMessage() + "");
            return "";
        }
    }

    //yyyy-MM-dd hh:mm:ss
    public static String getPostTimeAgoV2(String createdDate) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        try {
            Date dateCreated = sdf.parse(createdDate);

//            //here datecreated is UTC date convert it to device timezone
//            Calendar mCalendar = new GregorianCalendar();
//            TimeZone mTimeZone = mCalendar.getTimeZone();
//            int mGMTOffset = mTimeZone.getRawOffset();
//            System.out.printf("GMT offset is %s hours", TimeUnit.HOURS.convert(mGMTOffset, TimeUnit.MILLISECONDS));

            Date dateNow = new Date();
            dateNow.setTime(System.currentTimeMillis());
//            Logger.e("created VS current time", dateCreated.getTime() + " VS " + dateNow.getTime());

            long difference = dateNow.getTime() - dateCreated.getTime();
//            Logger.e("difference", difference + "");
//            int days = (int) (difference / (1000*60*60*24));
//            int hours = (int) ((difference - (1000*60*60*24*days)) / (1000*60*60));
//            int min = (int) (difference - (1000*60*60*24*days) - (1000*60*60*hours)) / (1000*60);
//            Logger.e("dd, hh, mm", days + " : " + hours + " : " + min);
//            hours = (hours < 0 ? -hours : hours);

//            if (days > 0){
//                return days + " days ago";
//            }else if (hours > 0){
//                return hours + " hours ago";
//            }else if (min > 0){
//                return min + " mins ago";
//            }else{
//                return "now";
//            }


            long seconds = difference / 1000;
            long minutes = seconds / 60;
            long hours = minutes / 60;
            long days = hours / 24;

            if (dateCreated.before(dateNow)) {

//                Logger.e("oldDate", "is previous date");
//                Logger.e("Difference: ", " seconds: " + seconds + " minutes: " + minutes
//                        + " hours: " + hours + " days: " + days);
            }

            if (days > 0) {
                return days + " days ago";
            } else if (hours > 0) {
                return hours + " hours ago";
            } else if (minutes > 0) {
                return minutes + " mins ago";
            } else if (seconds > 0) {
                return seconds + "sec ago";
            } else {
                return "now";
            }


        } catch (ParseException ex) {
            Logger.e("date diff pares ex", ex.getMessage() + "");
            return "now";
        }
    }

    public static String getPostTimeAgoV3(String createdDate) {
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss a");
        sdf.setTimeZone(TimeZone.getTimeZone("UTF"));
        try {
            Date dateCreated = sdf.parse(createdDate);
            Date dateNow = new Date();
            dateNow.setTime(System.currentTimeMillis());
//            Logger.e("created VS current time", dateCreated.getTime() + " VS " + dateNow.getTime());

            long difference = dateNow.getTime() - dateCreated.getTime();
//            Logger.e("difference", difference + "");
//            int days = (int) (difference / (1000*60*60*24));
//            int hours = (int) ((difference - (1000*60*60*24*days)) / (1000*60*60));
//            int min = (int) (difference - (1000*60*60*24*days) - (1000*60*60*hours)) / (1000*60);
//            Logger.e("dd, hh, mm", days + " : " + hours + " : " + min);
//            hours = (hours < 0 ? -hours : hours);

//            if (days > 0){
//                return days + " days ago";
//            }else if (hours > 0){
//                return hours + " hours ago";
//            }else if (min > 0){
//                return min + " mins ago";
//            }else{
//                return "now";
//            }


            long seconds = difference / 1000;
            long minutes = seconds / 60;
            long hours = minutes / 60;
            long days = hours / 24;

            if (dateCreated.before(dateNow)) {

//                Logger.e("oldDate", "is previous date");
//                Logger.e("Difference: ", " seconds: " + seconds + " minutes: " + minutes
//                        + " hours: " + hours + " days: " + days);
            }

            if (days > 0) {
                return days + " days ago";
            } else if (hours > 0) {
                return hours + " hours ago";
            } else if (minutes > 0) {
                return minutes + " mins ago";
            } else if (seconds > 0) {
                return seconds + "sec ago";
            } else {
                return "now";
            }


        } catch (ParseException ex) {
            Logger.e("date diff pares ex", ex.getMessage() + "");
            return "now";
        } catch (NullPointerException e) {
            Logger.e("date diff ex 11", e.getMessage() + "");
            return "";
        }
    }
}
