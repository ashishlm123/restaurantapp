package com.emts.restaurantapp.helper;

/**
 * Created by theone on 10/18/2016.
 */

public class Api {

    private static Api api;

    public static Api getInstance() {
        if (api != null) {
            return api;
        } else {
            api = new Api();
            return api;
        }

    }


    public final String apiKey = "EMTSappBYsuzanDON";

    private String getIp() {
        return "http://202.166.198.151:8888/lifterlifec/api/";
    }

    public final String registerUrl = getIp() + "users/signup";
    public final String loginUrl = getIp() + "users/login";
    public final String forgotPasswordUrl = getIp() + "users/forgot_password";
    public final String categoryUrl = getIp() + "api_general/cuisines";
    public final String todaysMenuUrl = getIp() + "api_general/todays_menu";
    public final String mostSellingUrl = getIp() + "api_general/most_selling_item";
    public final String searchFoodUrl = getIp() + "api_general/search_food";
    public final String cartUrl = getIp() + "add_to_cart/add_cart";
    public final String insideCartUrl = getIp() + "add_to_cart/index";
    public final String itemQuantityUpdateUrl = getIp() + "add_to_cart/update_cart";
    public final String placeOrderUrl = getIp() + "order/place_order";
    public final String orderHistoryUrl = getIp() + "order/order_history";
    public final String fbLoginUrl = getIp() + "users/facebook_login";
    public final String aboutUrl = getIp() + "api_general/about_us";
    public final String contactUsUrl = getIp() + "api_general/contact_us";
    public final String searchByCategoryurl = getIp() + "api_general/search_food_by_category";
    public final String deleteCartUrl = getIp() + "add_to_cart/delete_cart";
    public final String cartCountUrl = getIp() + "api_general/card_count";


}