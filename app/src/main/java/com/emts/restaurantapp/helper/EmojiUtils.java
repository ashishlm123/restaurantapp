package com.emts.restaurantapp.helper;

/**
 * Created by User on 2016-12-21.
 */

public class EmojiUtils {

    public static String escapeJavaString(String st) {
        StringBuilder builder = new StringBuilder();
        try {
            for (int i = 0; i < st.length(); i++) {
                char c = st.charAt(i);
                if (!Character.isLetterOrDigit(c) && !Character.isSpaceChar(c) && !Character.isWhitespace(c)) {
                    String unicode = String.valueOf(c);
                    int code = (int) c;
                    if (!(code >= 0 && code <= 255)) {
                        unicode = "\\\\u" + Integer.toHexString(c);
                    }
                    builder.append(unicode);
                } else {
                    builder.append(c);
                }
            }
            Logger.e("emoji escaped Unicode Block", builder.toString());
        } catch (Exception e) {
            Logger.e("emoji utils ex", e.getMessage() + " ");
        }
        return builder.toString();
    }

    public static String unescapeJava(String escaped) {
        if (!escaped.contains("\\u"))
            return escaped;

        String processed = "";

        int position = escaped.indexOf("\\u");
        while (position != -1) {
            if (position != 0) {
                processed += escaped.substring(0, position-1);
                Logger.e("unescape processed", processed + "  -");
            }
            String token = escaped.substring(position + 2, position + 6);
            Logger.e("unescaped token", token + "  -");
            escaped = escaped.substring(position + 6);
            Logger.e("unescaped toescape", escaped + "  -");
            processed += (char) Integer.parseInt(token, 16);
            position = escaped.indexOf("\\u");
        }
        processed += escaped;

        return processed;
    }
}
