package com.emts.restaurantapp.helper;

import android.text.TextUtils;

/**
 * Created by theone on 6/9/2017.
 */

public class TextHelper {

    public static String checkNull(String value) {
        if (TextUtils.isEmpty(value) || value.equalsIgnoreCase("null")) {
            return "N/A";
        }
        return value;
    }
}
