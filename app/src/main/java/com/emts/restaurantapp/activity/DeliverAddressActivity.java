package com.emts.restaurantapp.activity;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.emts.restaurantapp.R;
import com.emts.restaurantapp.helper.PreferenceHelper;

public class DeliverAddressActivity extends AppCompatActivity {

    Toolbar toolbar;
    EditText city, address, street, houseNo, mobileNo;
    Button save;
    PreferenceHelper helper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deliver_address);
        helper = PreferenceHelper.getInstance(this);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Edit Delivery Address");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        city = (EditText) findViewById(R.id.city);
        address = (EditText) findViewById(R.id.address);
        street = (EditText) findViewById(R.id.street);
        houseNo = (EditText) findViewById(R.id.house_no);
        mobileNo = (EditText) findViewById(R.id.mobile_no);

        Intent intent = getIntent();
        String ct = intent.getStringExtra("city");
        String ad = intent.getStringExtra("address");
        String st = intent.getStringExtra("street");
        String hn = intent.getStringExtra("house_no");
        String mn = intent.getStringExtra("mobile_no");

        if(!TextUtils.isEmpty(ct)&& !ct.equalsIgnoreCase("null")){
            city.setText(ct);
        }

        if(!TextUtils.isEmpty(ad)&& !ad.equalsIgnoreCase("null")){
            address.setText(ad);
        }

        if(!TextUtils.isEmpty(st)&& !st.equalsIgnoreCase("null")){
            street.setText(st);
        }

        if(!TextUtils.isEmpty(hn)&& !hn.equalsIgnoreCase("null")){
            houseNo.setText(hn);
        }

        if(!TextUtils.isEmpty(mn)&& !mn.equalsIgnoreCase("null")){
            mobileNo.setText(mn);
        }



        save = (Button) findViewById(R.id.save_address);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                    Intent intent = new Intent(getApplicationContext(), CheckOutActivity.class);
                    intent.putExtra("city", city.getText().toString().trim());
                    intent.putExtra("address", address.getText().toString().trim());
                    intent.putExtra("street", street.getText().toString().trim());
                    intent.putExtra("house_no", houseNo.getText().toString().trim());
                    intent.putExtra("mobile_no", mobileNo.getText().toString().trim());
                    setResult(RESULT_OK, intent);
                    finish();


            }
        });
    }


}
