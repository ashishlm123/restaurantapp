package com.emts.restaurantapp.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.dd.CircularProgressButton;
import com.emts.restaurantapp.MainActivity;
import com.emts.restaurantapp.R;
import com.emts.restaurantapp.helper.AlertUtils;
import com.emts.restaurantapp.helper.Api;
import com.emts.restaurantapp.helper.Logger;
import com.emts.restaurantapp.helper.NetworkUtils;
import com.emts.restaurantapp.helper.PreferenceHelper;
import com.emts.restaurantapp.helper.VolleyHelper;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;

public class LoginActivity extends AppCompatActivity {
    CircularProgressButton signIn, signInFb;
    EditText userName, passWord;
    PreferenceHelper helper;
    TextView forgetPassword, registerNow, errName, errPass;
    CallbackManager callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Initialization of facebook SDK
        helper = PreferenceHelper.getInstance(this);
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
        callbackManager = CallbackManager.Factory.create();
        String fBId = helper.getString(PreferenceHelper.FB_APP_ID, getString(R.string.facebook_app_id));
        FacebookSdk.setApplicationId(fBId);

        setContentView(R.layout.activity_login);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        toolbar.setTitle("Login");
        init();


        signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validation()) {
//                    signIn.setProgress(50);

                    if (NetworkUtils.isInNetwork(LoginActivity.this)) {
                        userLoginTask();

                    } else {
                        AlertUtils.showSnack(LoginActivity.this, view, "No internet connection...");
                    }
                    // turn on indeterminate progress
                    // set progress > 0 & < 100 to display indeterminate progress
//                signIn.setProgress(100); // set progress to 100 or -1 to indicate complete or error state
//                signIn.setProgress(0); // set progress to 0 to switch back to normal state
                } else {

                }
            }
        });

        signInFb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fbTask();
            }
        });

        forgetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), ForgotPasswordActivity.class);
                startActivity(intent);
            }
        });

        registerNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), RegisterActivity.class);
                startActivity(intent);
            }
        });
    }

    private void init() {
        userName = (EditText) findViewById(R.id.edt_username);
        errName = (TextView) findViewById(R.id.err_usrname);
        forgetPassword = (TextView) findViewById(R.id.forget_pasword);
        passWord = (EditText) findViewById(R.id.edt_password);
        errPass = (TextView) findViewById(R.id.err_password);
        registerNow = (TextView) findViewById(R.id.register_now);
        signIn = (CircularProgressButton) findViewById(R.id.btn_login);
        signIn.setIndeterminateProgressMode(true);
        signInFb = (CircularProgressButton) findViewById(R.id.btn_fb);
        signInFb.setIndeterminateProgressMode(true);
    }

    public boolean validation() {
        boolean isValid = true;
        if (TextUtils.isEmpty(userName.getText().toString().trim())) {
            isValid = false;
            errName.setVisibility(View.VISIBLE);
            errName.setText("This field is required...");
        } else {
            errName.setVisibility(View.GONE);
        }

        if (TextUtils.isEmpty(passWord.getText().toString().trim())) {
            isValid = false;
            errPass.setVisibility(View.VISIBLE);
            errPass.setText("This field is required...");
        } else {
            errPass.setVisibility(View.GONE);

        }

        return isValid;
    }


    //Method for Login with facebook
    private void fbTask() {
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().logOut();

        LoginManager.getInstance().logInWithReadPermissions(LoginActivity.this, (Arrays.asList("email",
                "public_profile")));
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        Logger.e("facebook login response", loginResult.toString() + " &&\n "
                                + loginResult.getAccessToken() + "\n" + loginResult.getRecentlyGrantedPermissions()
                                + "\n" + loginResult.getRecentlyDeniedPermissions() + "\n");

                        GraphRequest request = GraphRequest.newMeRequest(
                                loginResult.getAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(
                                            JSONObject object, GraphResponse response) {
                                        Logger.e("object", object.toString());
                                        try {
                                            final String fb_id = object.optString("id");
                                            final String name = object.optString("name");
                                            final String first_name = object.optString("first_name");
                                            final String last_name = object.optString("last_name");
                                            final String age = object.optString("age_range");
                                            final String picture = object.optString("picture");
                                            final String email = object.optString("email"); //Get null value here
                                            final String gender = object.optString("gender");
                                            final String username = "";
                                            final String device_id = "";


                                            Logger.e("Facebook Data", fb_id + "\n" + name + "\n" + first_name
                                                    + "\n" + last_name + "\n" + email + "\n" + age + "\n" + gender +
                                                    "\n" + picture + "\n");
                                            LoginManager.getInstance().logOut();



                                            String birthday = "";


                                            if (email.equals("")) {
                                                LayoutInflater inflater = getLayoutInflater();
                                                View alert_email = inflater.inflate(R.layout.alert_enter_email, null);
                                                final EditText enterEmail = alert_email.findViewById(R.id.email_address);
                                                Button submit = alert_email.findViewById(R.id.submit);
                                                final TextView errorText = alert_email.findViewById(R.id.error_text);
                                                submit.setOnClickListener(new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View view) {

                                                        if (Patterns.EMAIL_ADDRESS.matcher(enterEmail.getText().toString().trim()).matches()) {
                                                            errorText.setVisibility(View.GONE);
                                                            if (NetworkUtils.isInNetwork(getApplicationContext())) {
                                                                userFbLoginTask(fb_id, enterEmail.getText().toString().trim(),
                                                                        device_id, username, gender, name);

                                                            } else {
                                                                AlertUtils.showSnack(LoginActivity.this, view, "No internet connection...");
                                                            }

                                                        } else {
                                                            errorText.setText("Invalid Email Address");
                                                            errorText.setVisibility(View.VISIBLE);
                                                        }
                                                    }

                                                });
                                                AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                                                builder.setView(alert_email);
                                                builder.show();

                                            } else {
                                                if (NetworkUtils.isInNetwork(getApplicationContext())) {
                                                    userFbLoginTask(fb_id, email, device_id, username, gender, name);
                                                } else {
                                                    AlertUtils.showSnack(LoginActivity.this, passWord, "No internet connection...");
                                                }
                                            }


                                        } catch (Exception e) {
                                            LoginManager.getInstance().logOut();
                                        }
                                    }
                                });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id,name,first_name,last_name,email,gender," +
                                "age_range, picture");
                        request.setParameters(parameters);
                        request.executeAsync();
                    }

                    @Override
                    public void onCancel() {
                        // App code
                        Logger.e("cancell", "called");
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                        Logger.e("error", exception.getMessage() + "\n" + exception.getCause());
                    }
                });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void userLoginTask() {
        final ProgressDialog dialog = AlertUtils.showProgressDialog(LoginActivity.this, "Logging In...");

        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postMap = vHelper.getPostParams();
        postMap.put("username", userName.getText().toString().trim());
        postMap.put("password", passWord.getText().toString().trim());

        vHelper.addVolleyRequestListeners(Api.getInstance().loginUrl, Request.Method.POST, postMap,
                new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        dialog.dismiss();
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                JSONObject user = res.getJSONObject("user_detail");
                                helper.edit().putString(PreferenceHelper.APP_USER_NAME, user.getString("username")).commit();
                                helper.edit().putString(PreferenceHelper.APP_USER_ID, user.getString("id")).commit();
                                helper.edit().putString(PreferenceHelper.APP_USER_EMAIL, user.getString("email")).commit();
                                helper.edit().putBoolean(PreferenceHelper.IS_LOGIN, true).commit();
                                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);

                            } else {
                                dialog.dismiss();
                                AlertUtils.showSnack(LoginActivity.this, userName, res.getString("message"));
                            }
                        } catch (JSONException e) {
                            Logger.e("userLoginTask json ex", e.getMessage() + "");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError error) {
                        try {
                            dialog.dismiss();
                            JSONObject errorObj = new JSONObject(new String(error.networkResponse.data));
                            AlertUtils.showSnack(LoginActivity.this, forgetPassword, errorObj.getString("message"));
                        } catch (Exception e) {
                            if (error instanceof NetworkError) {
                                Toast.makeText(getApplicationContext(), "No Internet Connection...", Toast.LENGTH_SHORT).show();
                            } else if (error instanceof ServerError) {
                                Toast.makeText(getApplicationContext(), "Server Error...", Toast.LENGTH_SHORT).show();
                            } else if (error instanceof AuthFailureError) {
                                Toast.makeText(getApplicationContext(), "Authentication Error...", Toast.LENGTH_SHORT).show();
                            } else if (error instanceof ParseError) {
                                Toast.makeText(getApplicationContext(), "Parse Error", Toast.LENGTH_SHORT).show();
                            } else if (error instanceof TimeoutError) {
                                Toast.makeText(getApplicationContext(), "Timed Out", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }, "userLoginTask");
    }

    private void userFbLoginTask(String fb_id, String email, String device_id,
                                 String username, String gender, String name) {
        final ProgressDialog dialog = AlertUtils.showProgressDialog(LoginActivity.this, "Logging In With Facebook...\n Please Wait...");
        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postMap = vHelper.getPostParams();

        postMap.put("facebook_id", fb_id);
        postMap.put("email", email);
        postMap.put("device_id", device_id);
        postMap.put("username", username);
        postMap.put("gender", gender);
        postMap.put("name", name);

        vHelper.addVolleyRequestListeners(Api.getInstance().fbLoginUrl, Request.Method.POST, postMap,
                new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        dialog.dismiss();
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                JSONObject user = res.getJSONObject("user_detail");
                                helper.edit().putString(PreferenceHelper.APP_USER_ID, user.getString("id")).commit();
                                helper.edit().putString(PreferenceHelper.APP_USER_EMAIL, user.getString("email")).commit();
                                helper.edit().putBoolean(PreferenceHelper.IS_LOGIN, true).commit();
                                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                            } else {
                                dialog.dismiss();
                                AlertUtils.showSnack(LoginActivity.this, userName, res.getString("message"));
                            }
                        } catch (JSONException e) {
                            Logger.e("userFbLoginTask json ex", e.getMessage() + "");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError error) {
                        try {
                            dialog.dismiss();
                            JSONObject errorObj = new JSONObject(new String(error.networkResponse.data));
                            AlertUtils.showSnack(LoginActivity.this, forgetPassword, errorObj.getString("message"));
                        } catch (Exception e) {
                            if (error instanceof NetworkError) {
                                Toast.makeText(getApplicationContext(), "No Internet Connection...", Toast.LENGTH_SHORT).show();
                            } else if (error instanceof ServerError) {
                                Toast.makeText(getApplicationContext(), "Server Error...", Toast.LENGTH_SHORT).show();
                            } else if (error instanceof AuthFailureError) {
                                Toast.makeText(getApplicationContext(), "Authentication Error...", Toast.LENGTH_SHORT).show();
                            } else if (error instanceof ParseError) {
                                Toast.makeText(getApplicationContext(), "Parse Error", Toast.LENGTH_SHORT).show();
                            } else if (error instanceof TimeoutError) {
                                Toast.makeText(getApplicationContext(), "Timed Out", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }, "userFbLoginTask");


    }

}
