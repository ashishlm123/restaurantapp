package com.emts.restaurantapp.activity;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.emts.restaurantapp.MainActivity;
import com.emts.restaurantapp.R;
import com.emts.restaurantapp.adapter.CartAdapter;
import com.emts.restaurantapp.helper.AlertUtils;
import com.emts.restaurantapp.helper.Api;
import com.emts.restaurantapp.helper.Logger;
import com.emts.restaurantapp.helper.NetworkUtils;
import com.emts.restaurantapp.helper.PreferenceHelper;
import com.emts.restaurantapp.helper.VolleyHelper;
import com.emts.restaurantapp.models.CartModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class CartActivity extends AppCompatActivity {
    CartAdapter cartAdapter;
    ArrayList<CartModel> cartList;
    RecyclerView cartListing;
    Toolbar toolbar;
    Button addItems, checkOut;
    TextView foodName, itemPrice, itemTotalPrice, wholePrice;
    PreferenceHelper helper;
    ProgressBar progressBar;
    TextView errText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        helper = PreferenceHelper.getInstance(this);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("My Cart");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        cartList = new ArrayList<>();
        cartListing = (RecyclerView) findViewById(R.id.cart_recycler);
        cartListing.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        cartListing.setNestedScrollingEnabled(false);
        cartAdapter = new CartAdapter(CartActivity.this, cartList);
        cartListing.setAdapter(cartAdapter);

        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        errText = (TextView) findViewById(R.id.error_text);

        if (NetworkUtils.isInNetwork(getApplicationContext())) {
            userInsideCartTask();
        } else {
            errText.setText("No Internet Connection...");
            errText.setVisibility(View.VISIBLE);
            cartListing.setVisibility(View.GONE);
        }


        foodName = (TextView) findViewById(R.id.food_name);
        itemPrice = (TextView) findViewById(R.id.food_price);
        itemTotalPrice = (TextView) findViewById(R.id.total_price);
        wholePrice = (TextView) findViewById(R.id.whole_price);


        checkOut = (Button) findViewById(R.id.btn_checkOut);
        checkOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cartList.size() > 0) {
                    Intent intent = new Intent(getApplicationContext(), CheckOutActivity.class);
                    intent.putExtra("listcart", cartList);
                    startActivity(intent);
                } else{
                    AlertUtils.showSnack(getApplicationContext(),view, "Empty Cart...");
                }
            }
        });

        addItems = (Button) findViewById(R.id.btn_addItems);
        addItems.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
            }
        });


    }

    private void userInsideCartTask() {
        progressBar.setVisibility(View.VISIBLE);
        errText.setVisibility(View.GONE);
        cartListing.setVisibility(View.GONE);

        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postMap = vHelper.getPostParams();
        postMap.put("user_id", helper.getUserId());


        vHelper.addVolleyRequestListeners(Api.getInstance().insideCartUrl, Request.Method.POST, postMap,
                new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                JSONArray cartArray = res.getJSONArray("cart");
                                if (cartArray.length() == 0) {
                                    cartMessage();
                                    return;
                                }
                                for (int i = 0; i < cartArray.length(); i++) {
                                    JSONObject jsonObject = cartArray.getJSONObject(i);
                                    CartModel cartModel = new CartModel();
                                    cartModel.setCartId(jsonObject.getString("id"));
                                    cartModel.setProductId(jsonObject.getString("product_id"));
                                    cartModel.setInvoice_id(jsonObject.getString("invoice_id"));
                                    cartModel.setFoodName(jsonObject.getString("name"));
                                    int quantity = Integer.parseInt(jsonObject.getString("quantity"));
                                    cartModel.setQty(quantity);
                                    double price = Double.parseDouble(jsonObject.getString("price"));
                                    cartModel.setFoodPrice(price);
                                    cartModel.setTotalPrice(quantity * price);
                                    cartModel.setFoodImage(jsonObject.getString("image"));
                                    cartList.add(cartModel);
                                }
                                wholePrice.setText(String.valueOf("$ " + Double.parseDouble(res.getString("sum"))));
                                cartAdapter.notifyDataSetChanged();
                                cartListing.setVisibility(View.VISIBLE);
                                errText.setVisibility(View.GONE);
                            } else {
                                errText.setVisibility(View.VISIBLE);
                                errText.setText(res.getString("message"));
                                cartListing.setVisibility(View.GONE);
                            }


                        } catch (JSONException e) {
                            Logger.e("userInsideCartTask json ex", e.getMessage() + "");
                        }
                        progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError error) {
                        progressBar.setVisibility(View.GONE);
                        try {
                            JSONObject errorObj = new JSONObject(new String(error.networkResponse.data));
                            AlertUtils.showSnack(getApplicationContext(), foodName, errorObj.getString("message"));
                        } catch (Exception e) {
                            if (error instanceof NetworkError) {
                                Toast.makeText(getApplicationContext(), "No Internet Connection...", Toast.LENGTH_SHORT).show();
                            } else if (error instanceof ServerError) {
                                Toast.makeText(getApplicationContext(), "Server Error...", Toast.LENGTH_SHORT).show();
                            } else if (error instanceof AuthFailureError) {
                                Toast.makeText(getApplicationContext(), "Authentication Error...", Toast.LENGTH_SHORT).show();
                            } else if (error instanceof ParseError) {
                                Toast.makeText(getApplicationContext(), "Parse Error", Toast.LENGTH_SHORT).show();
                            } else if (error instanceof TimeoutError) {
                                Toast.makeText(getApplicationContext(), "Timed Out", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }, "userInsideCartTask");
    }

    public void cartMessage() {
        errText.setText("Cart Is Empty..");
        cartListing.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);
        errText.setVisibility(View.VISIBLE);
    }

    public void updateWholePrice() {
        Double total = 0.0;
        for (int i = 0; i < cartList.size(); i++) {
            CartModel cartModel = cartList.get(i);
            total = total + cartModel.getTotalPrice();
        }
        wholePrice.setText("$ " + String.valueOf(total));
    }

}
