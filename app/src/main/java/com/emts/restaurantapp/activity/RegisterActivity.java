package com.emts.restaurantapp.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.dd.CircularProgressButton;
import com.emts.restaurantapp.MainActivity;
import com.emts.restaurantapp.R;
import com.emts.restaurantapp.helper.AlertUtils;
import com.emts.restaurantapp.helper.Api;
import com.emts.restaurantapp.helper.Logger;
import com.emts.restaurantapp.helper.NetworkUtils;
import com.emts.restaurantapp.helper.PreferenceHelper;
import com.emts.restaurantapp.helper.VolleyHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class RegisterActivity extends AppCompatActivity {

    PreferenceHelper helper;
    CircularProgressButton signUp;
    Toolbar toolbar;
    EditText userName, eMail, passWord, conPass;
    TextView errUsr, errEm, errPass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        helper = PreferenceHelper.getInstance(this);
        init();


        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (validation()) {
                    // turn on indeterminate progress
//                    signUp.setProgress(50);
                    if (NetworkUtils.isInNetwork(RegisterActivity.this)) {
                        userSignUpTask();

                    } else{
                        AlertUtils.showSnack(RegisterActivity.this, view , "No internet connection...");
                    }

                }
            }
        });

        toolbar.setTitle("Register Now");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


    }

    private void userSignUpTask() {
        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postMap = vHelper.getPostParams();
        postMap.put("username", userName.getText().toString().trim());
        postMap.put("email", eMail.getText().toString().trim());
        postMap.put("password", passWord.getText().toString().trim());


        vHelper.addVolleyRequestListeners(Api.getInstance().registerUrl, Request.Method.POST, postMap,
                new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                if (res.getString("login").equals("yes")) {
                                    JSONObject user = res.getJSONObject("user_detail");
                                    helper.edit().putString(PreferenceHelper.APP_USER_NAME, user.getString("username")).commit();
                                    helper.edit().putString(PreferenceHelper.APP_USER_ID, user.getString("id")).commit();
                                    helper.edit().putString(PreferenceHelper.APP_USER_EMAIL, user.getString("email")).commit();
                                    helper.edit().putBoolean(PreferenceHelper.IS_LOGIN, true).commit();
                                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                    startActivity(intent);


                                } else {
                                    AlertDialog.Builder builder = new AlertDialog.Builder(RegisterActivity.this);
                                    builder.setMessage(res.getString("message"));
                                    builder.setPositiveButton("Login Now", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                                            startActivity(intent);
                                        }
                                    });
                                    builder.show();
                                }

                            } else {
                                AlertUtils.showSnack(RegisterActivity.this, eMail, res.getString("message"));
                            }
                        } catch (JSONException e) {
                            Logger.e("userSignUpTask json ex", e.getMessage() + "");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError error) {
                        try {
                            JSONObject errorObj = new JSONObject(new String(error.networkResponse.data));
                            AlertUtils.showSnack(RegisterActivity.this, userName, errorObj.getString("message"));
                        } catch (Exception e) {
                            if (error instanceof NetworkError) {
                                Toast.makeText(getApplicationContext(), "No Internet Connection...", Toast.LENGTH_SHORT).show();
                            } else if (error instanceof ServerError) {
                                Toast.makeText(getApplicationContext(), "Server Error...", Toast.LENGTH_SHORT).show();
                            } else if (error instanceof AuthFailureError) {
                                Toast.makeText(getApplicationContext(), "Authentication Error...", Toast.LENGTH_SHORT).show();
                            } else if (error instanceof ParseError) {
                                Toast.makeText(getApplicationContext(), "Parse Error", Toast.LENGTH_SHORT).show();
                            } else if (error instanceof TimeoutError) {
                                Toast.makeText(getApplicationContext(), "Timed Out", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }, "userSignUpTask");


    }

    private void init() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        userName = (EditText) findViewById(R.id.edt_username);
        eMail = (EditText) findViewById(R.id.edt_email);
        passWord = (EditText) findViewById(R.id.edt_password);
        conPass = (EditText) findViewById(R.id.edt_conpassword);
        errUsr = (TextView) findViewById(R.id.err_usrname);
        errEm = (TextView) findViewById(R.id.err_email);
        errPass = (TextView) findViewById(R.id.err_password);
        signUp = (CircularProgressButton) findViewById(R.id.btn_signup);
        signUp.setIndeterminateProgressMode(true);

    }

    public boolean validation() {
        boolean isValid = true;
        if (TextUtils.isEmpty(userName.getText().toString().trim())) {
            isValid = false;
            errUsr.setVisibility(View.VISIBLE);
            errUsr.setText("This field is required...");
        } else {
            errUsr.setVisibility(View.GONE);
        }

        //For Email Address
        if (TextUtils.isEmpty(eMail.getText().toString().trim())) {
            isValid = false;
            errEm.setVisibility(View.VISIBLE);
            errEm.setText("This Field is required...");
        } else {
            if (Patterns.EMAIL_ADDRESS.matcher(eMail.getText().toString().trim()).matches()) {
                errEm.setVisibility(View.GONE);
            } else {
                Toast.makeText(RegisterActivity.this, "Invalid Email Address", Toast.LENGTH_SHORT).show();
            }
        }

        //For Password
        if (passWord.getText().toString().trim().equals("")) {
            isValid = false;
            errPass.setVisibility(View.VISIBLE);
            errPass.setText("This Field is required...");

        } else {
            if (passWord.getText().toString().trim().length() > 6) {
                if (passWord.getText().toString().trim().equals(conPass.getText().toString().trim())) {
                    errPass.setVisibility(View.GONE);
                } else {
                    isValid = false;
                    errPass.setText("Password do not match");
                    errPass.setVisibility(View.VISIBLE);
                }
            } else {
                isValid = false;
                errPass.setText("Password should have more than six characters.");
                errPass.setVisibility(View.VISIBLE);
            }
        }

        return isValid;
    }
}
