package com.emts.restaurantapp.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.emts.restaurantapp.R;
import com.emts.restaurantapp.helper.AlertUtils;
import com.emts.restaurantapp.helper.Api;
import com.emts.restaurantapp.helper.Logger;
import com.emts.restaurantapp.helper.NetworkUtils;
import com.emts.restaurantapp.helper.PreferenceHelper;
import com.emts.restaurantapp.helper.VolleyHelper;
import com.emts.restaurantapp.models.FoodMdl;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class FoodDetailsActivity extends AppCompatActivity {

    ImageView foodIcon;
    TextView foodName, ingredients, foodPrice, minus, plus, qty;
    EditText spclReq;
    Button addToCart;
    Toolbar toolbar;
    FoodMdl foodMdl;
    PreferenceHelper helper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food_details);
        helper = PreferenceHelper.getInstance(this);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        toolbar.setTitle("Item Details");

        Intent intent = getIntent();
        foodMdl = (FoodMdl) intent.getSerializableExtra("item");
        init();


        minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Integer a = Integer.parseInt(String.valueOf(qty.getText()));
                if (a > 1) {
                    a--;
                } else {
                    a = 1;
                }
                qty.setText(String.valueOf(a));
            }
        });

        plus.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View view) {
                Integer a = Integer.parseInt(String.valueOf(qty.getText()));
                a++;
                qty.setText(String.valueOf(a));
            }
        });


        addToCart.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View view) {
                if (NetworkUtils.isInNetwork(getApplicationContext())) {
                    Log.e("Cart ma Aaipugyo?", "Ah, Balla Ball Aayipugyo Ghumera");
                    userCartTask();
                } else {
                    AlertUtils.showSnack(FoodDetailsActivity.this, view, "No Internet Connection...");
                }
            }
        });
    }

    private void init() {
        foodIcon = (ImageView) findViewById(R.id.food_icon);
        foodName = (TextView) findViewById(R.id.food_name);
        ingredients = (TextView) findViewById(R.id.ingredients);
        foodPrice = (TextView) findViewById(R.id.food_price);
        spclReq = (EditText) findViewById(R.id.spclReq);
        minus = (TextView) findViewById(R.id.minus);
        plus = (TextView) findViewById(R.id.plus);
        qty = (TextView) findViewById(R.id.qty);
        addToCart = (Button) findViewById(R.id.btn_addCart);

        if (!TextUtils.isEmpty(foodMdl.getFoodImage())) {
            Picasso.with(this).load(foodMdl.getFoodImage()).into(foodIcon);
        }
        foodName.setText(foodMdl.getFoodName());
        foodPrice.setText("$ " + String.valueOf(foodMdl.getFoodPrice()));
        ingredients.setText(foodMdl.getIngredients());
    }

    private void userCartTask() {
        final ProgressDialog dialog = AlertUtils.showProgressDialog(FoodDetailsActivity.this, "Please Wait...");

        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postMap = vHelper.getPostParams();
        postMap.put("user_id", helper.getUserId());
        postMap.put("product_id", foodMdl.getProductId());
        postMap.put("special_request", spclReq.getText().toString().trim());
        postMap.put("quantity", qty.getText().toString().trim());


        vHelper.addVolleyRequestListeners(Api.getInstance().cartUrl, Request.Method.POST, postMap,
                new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        dialog.dismiss();
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                Snackbar mSnackbar = Snackbar.make(addToCart, res.getString("message"),
                                        Snackbar.LENGTH_INDEFINITE).setAction("View Cart", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        Intent intent = new Intent(getApplicationContext(), CartActivity.class);
                                        startActivity(intent);
                                    }
                                });
                                dialog.dismiss();
                                mSnackbar.show();


                            } else {
                                Snackbar mSnackbar = Snackbar.make(addToCart, Html.fromHtml(res.getString("message")).toString(),
                                        Snackbar.LENGTH_INDEFINITE).setAction("View Cart", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        Intent intent = new Intent(getApplicationContext(), CartActivity.class);
                                        startActivity(intent);
                                    }
                                });
                                dialog.dismiss();
                                mSnackbar.show();
//                                AlertUtils.showSnack(FoodDetailsActivity.this, foodName, Html.fromHtml(res.getString("message")).toString());
                            }
                        } catch (JSONException e) {
                            Logger.e("userCartTask json ex", e.getMessage() + "");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError error) {
                        dialog.dismiss();
                        try {
                            JSONObject errorObj = new JSONObject(new String(error.networkResponse.data));
                            AlertUtils.showSnack(FoodDetailsActivity.this, foodName, errorObj.getString("message"));
                        } catch (Exception e) {
                            if (error instanceof NetworkError) {
                                Toast.makeText(getApplicationContext(), "No Internet Connection...", Toast.LENGTH_SHORT).show();
                            } else if (error instanceof ServerError) {
                                Toast.makeText(getApplicationContext(), "Server Error...", Toast.LENGTH_SHORT).show();
                            } else if (error instanceof AuthFailureError) {
                                Toast.makeText(getApplicationContext(), "Authentication Error...", Toast.LENGTH_SHORT).show();
                            } else if (error instanceof ParseError) {
                                Toast.makeText(getApplicationContext(), "Parse Error", Toast.LENGTH_SHORT).show();
                            } else if (error instanceof TimeoutError) {
                                Toast.makeText(getApplicationContext(), "Timed Out", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }, "userCartTask");
    }

}
