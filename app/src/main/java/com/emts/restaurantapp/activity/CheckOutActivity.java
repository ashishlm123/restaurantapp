package com.emts.restaurantapp.activity;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.restaurantapp.R;
import com.emts.restaurantapp.adapter.CheckOutAdapter;
import com.emts.restaurantapp.helper.AlertUtils;
import com.emts.restaurantapp.helper.Api;
import com.emts.restaurantapp.helper.NetworkUtils;
import com.emts.restaurantapp.helper.PreferenceHelper;
import com.emts.restaurantapp.helper.VolleyHelper;
import com.emts.restaurantapp.models.CartModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class CheckOutActivity extends AppCompatActivity {
    ArrayList<CartModel> orderedList;
    //    CartModel cartModel;
    private static final int ADDRESS_RESULT_CODE = 0;
    PreferenceHelper helper;

    Double total;
    Button placeOrder;
    RadioButton cashRadio;
    Toolbar toolbar;
    TextView subTotalPrice, addAddress, totalAmt, payByCash, deliveryFee;
    TextView city, address, street, houseNo, mobileNo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_out);
        helper = PreferenceHelper.getInstance(this);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        toolbar.setTitle("Check Out");
        orderedList = (ArrayList<CartModel>) getIntent().getSerializableExtra("listcart");

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.order_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerView.setNestedScrollingEnabled(false);
        CheckOutAdapter checkOutAdapter = new CheckOutAdapter(this, orderedList);
        recyclerView.setAdapter(checkOutAdapter);

        city = (TextView) findViewById(R.id.city);
        address = (TextView) findViewById(R.id.address);
        street = (TextView) findViewById(R.id.street);
        houseNo = (TextView) findViewById(R.id.house_no);
        mobileNo = (TextView) findViewById(R.id.mobile_no);

        cashRadio = (RadioButton) findViewById(R.id.radio_cash);
        cashRadio.setChecked(true);


        subTotalPrice = (TextView) findViewById(R.id.sub_total_price);
        total = 0.0;
        for (int i = 0; i < orderedList.size(); i++) {
            CartModel cartModel = orderedList.get(i);
            total = total + cartModel.getTotalPrice();
        }
        subTotalPrice.setText("$ " + String.format("%.2f", total));

        deliveryFee = (TextView) findViewById(R.id.delivery_price);
        deliveryFee.setText("$ 0.00");

        totalAmt = (TextView) findViewById(R.id.total_price);
        totalAmt.setText("$ " + String.format("%.2f", total));

        payByCash = (TextView) findViewById(R.id.pay_by_cash);
        payByCash.setText("$ " + String.format("%.2f", total));


        addAddress = (TextView) findViewById(R.id.change_addr);

        addAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String add = addAddress.getText().toString().trim();
                Intent intent = new Intent(getApplicationContext(), DeliverAddressActivity.class);
                if (!add.equals("ADD")) {
                    intent.putExtra("city", city.getText().toString().trim());
                    intent.putExtra("address", address.getText().toString().trim());
                    intent.putExtra("street", street.getText().toString().trim());
                    intent.putExtra("house_no", houseNo.getText().toString().trim());
                    intent.putExtra("mobile_no", mobileNo.getText().toString().trim());
                }
                startActivityForResult(intent, ADDRESS_RESULT_CODE);
            }
        });

        cashRadio = (RadioButton) findViewById(R.id.radio_cash);

        placeOrder = (Button) findViewById(R.id.place_order);
        placeOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String add = addAddress.getText().toString().trim();
                Toast.makeText(CheckOutActivity.this, "Payment: To:Do", Toast.LENGTH_SHORT).show();

                if (add.equals("ADD")) {
                    AlertUtils.showSnack(CheckOutActivity.this, view, "Please, Update The Delivery Address First...");
                } else {
                    if (NetworkUtils.isInNetwork(getApplicationContext())) {
                        userPlaceOrderTask();
                    } else {
                        AlertUtils.showSnack(CheckOutActivity.this, view, "No Internet Connection...");
                    }
                }
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ADDRESS_RESULT_CODE) {
            if (resultCode == RESULT_OK) {


                String ct = data.getStringExtra("city");
                String ad = data.getStringExtra("address");
                String st = data.getStringExtra("street");
                String hn = data.getStringExtra("house_no");
                String mn = data.getStringExtra("mobile_no");

                city.setText(ct);
                address.setText(ad);
                street.setText(st);
                houseNo.setText(hn);
                mobileNo.setText(mn);

                addAddress.setText("EDIT");


            }
        }
    }

    private void userPlaceOrderTask() {

        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postMap = vHelper.getPostParams();
        postMap.put("user_id", helper.getUserId());
        postMap.put("total_cost", total.toString());
        postMap.put("delivery_city", city.getText().toString());
        postMap.put("delivery_mobile", mobileNo.getText().toString());
        postMap.put("delivery_address", address.getText().toString());
        postMap.put("delivery_street", street.getText().toString());
        postMap.put("delivery_house_number", houseNo.getText().toString());
        postMap.put("shipping_charge", String.valueOf(4)); // TODO delivery charge


        Double total_cost = 0.0;
        for (int i = 0; i < orderedList.size(); i++) {
            CartModel cartModel = orderedList.get(i);
            postMap.put("invoice_id", cartModel.getInvoice_id());
            postMap.put("product_id[" + i + "]", cartModel.getProductId());
            postMap.put("quantity[" + i + "]", String.valueOf(cartModel.getQty()));
            total_cost = total_cost + cartModel.getTotalPrice();

        }
        postMap.put("total_cost", String.valueOf(total_cost));


        vHelper.addVolleyRequestListeners(Api.getInstance().placeOrderUrl, Request.Method.POST, postMap,
                new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        AlertUtils.showAlertMessage(CheckOutActivity.this,"Place Order",  "Your order has been placed. \n View Your Order History.");
                        try {

                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {

                            }
                        } catch (JSONException e) {

                        }

                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {

                    }
                }, "userPlaceOrderTask");

    }


}
