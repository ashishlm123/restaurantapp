package com.emts.restaurantapp.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.dd.CircularProgressButton;
import com.emts.restaurantapp.R;
import com.emts.restaurantapp.helper.AlertUtils;
import com.emts.restaurantapp.helper.Api;
import com.emts.restaurantapp.helper.Logger;
import com.emts.restaurantapp.helper.NetworkUtils;
import com.emts.restaurantapp.helper.VolleyHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class ForgotPasswordActivity extends AppCompatActivity {

    CircularProgressButton resetPassword;
    Toolbar toolbar;
    EditText eMail;
    TextView errEm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        toolbar.setTitle("Reset Password");

        eMail = (EditText) findViewById(R.id.edt_email);
        errEm = (TextView) findViewById(R.id.err_email);

        resetPassword = (CircularProgressButton) findViewById(R.id.btn_sendresetcode);
        resetPassword.setIndeterminateProgressMode(true);

        resetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validation()) {
                    // turn on indeterminate progress
                    if (NetworkUtils.isInNetwork(ForgotPasswordActivity.this)) {
                        userForgotPasswordTask();

                    } else {
                        AlertUtils.showSnack(ForgotPasswordActivity.this, view, "No internet connection...");
                    }

//                    resetPassword.setProgress(50);// set progress > 0 & < 100 to display indeterminate progress
//                resetPassword.setProgress(100); // set progress to 100 or -1 to indicate complete or error state
//                resetPassword.setProgress(0); // set progress to 0 to switch back to normal state
                }
            }
        });

    }

    public boolean validation() {
        boolean isValid = true;

        //For Email Address
        if (TextUtils.isEmpty(eMail.getText().toString().trim())) {
            isValid = false;
            errEm.setVisibility(View.VISIBLE);
            errEm.setText("This Field is required...");
        } else {
            if (Patterns.EMAIL_ADDRESS.matcher(eMail.getText().toString().trim()).matches()) {
                errEm.setVisibility(View.GONE);
            } else {
                Toast.makeText(ForgotPasswordActivity.this, "Invalid Email Address", Toast.LENGTH_SHORT).show();
            }
        }

        return isValid;
    }

    private void userForgotPasswordTask() {
        final ProgressDialog dialog = AlertUtils.showProgressDialog(ForgotPasswordActivity.this, "Please Wait...");
        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postMap = vHelper.getPostParams();
        postMap.put("email", eMail.getText().toString().trim());


        vHelper.addVolleyRequestListeners(Api.getInstance().forgotPasswordUrl, Request.Method.POST, postMap,
                new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        dialog.dismiss();
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                AlertUtils.showErrorAlert(ForgotPasswordActivity.this, res.getString("message"), new AlertUtils.OnAlertButtonClickListener() {
                                    @Override
                                    public void onAlertButtonClick(boolean isPositiveButton) {
                                        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                                        startActivity(intent);
                                    }
                                });

                            } else {
                                dialog.dismiss();
                                AlertUtils.showSnack(ForgotPasswordActivity.this, eMail, res.getString("message"));
                            }
                        } catch (JSONException e) {
                            Logger.e("userForgotPasswordTask json ex", e.getMessage() + "");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError error) {
                        try {
                            dialog.dismiss();
                            JSONObject errorObj = new JSONObject(new String(error.networkResponse.data));
                            AlertUtils.showSnack(ForgotPasswordActivity.this, eMail, errorObj.getString("message"));
                        } catch (Exception e) {
                            if (error instanceof NetworkError) {
                                Toast.makeText(getApplicationContext(), "No Internet Connection...", Toast.LENGTH_SHORT).show();
                            } else if (error instanceof ServerError) {
                                Toast.makeText(getApplicationContext(), "Server Error...", Toast.LENGTH_SHORT).show();
                            } else if (error instanceof AuthFailureError) {
                                Toast.makeText(getApplicationContext(), "Authentication Error...", Toast.LENGTH_SHORT).show();
                            } else if (error instanceof ParseError) {
                                Toast.makeText(getApplicationContext(), "Parse Error", Toast.LENGTH_SHORT).show();
                            } else if (error instanceof TimeoutError) {
                                Toast.makeText(getApplicationContext(), "Timed Out", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }, "userForgotPasswordTask");
    }

}
