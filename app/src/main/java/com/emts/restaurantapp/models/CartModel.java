package com.emts.restaurantapp.models;

import java.io.Serializable;

/**
 * Created by Ashish on 26/07/2017.
 */

public class CartModel extends FoodMdl implements Serializable {

    private double totalPrice;
    private int qty;
    private String cartId;

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public String getCartId() {
        return cartId;
    }

    public void setCartId(String cartId) {
        this.cartId = cartId;
    }
}
