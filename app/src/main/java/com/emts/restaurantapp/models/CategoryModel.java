package com.emts.restaurantapp.models;

import java.io.Serializable;

/**
 * Created by Ashish on 25/07/2017.
 */

public class CategoryModel implements Serializable{
    private String cuisineName,id,desc;

    public String getCuisineName() {
        return cuisineName;
    }

    public void setCuisineName(String cuisineName) {
        this.cuisineName = cuisineName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
