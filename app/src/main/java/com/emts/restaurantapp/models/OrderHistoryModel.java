package com.emts.restaurantapp.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Prabin on 8/11/2017.
 */

public class OrderHistoryModel implements Serializable {
    private String dateTime, invoice_id, status, address, city;
    private double totalPrice;
    private ArrayList<CartModel> cartList;

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getInvoice_id() {
        return invoice_id;
    }

    public void setInvoice_id(String invoice_id) {
        this.invoice_id = invoice_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public ArrayList<CartModel> getCartList() {
        return cartList;
    }

    public void setCartList(ArrayList<CartModel> cartList) {
        this.cartList = cartList;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }
}
