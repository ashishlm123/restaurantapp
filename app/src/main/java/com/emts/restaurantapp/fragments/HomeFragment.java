package com.emts.restaurantapp.fragments;

import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.Nullable;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.emts.restaurantapp.R;
import com.emts.restaurantapp.adapter.CategoryAdapter;
import com.emts.restaurantapp.adapter.PageAdapter;
import com.emts.restaurantapp.helper.Api;
import com.emts.restaurantapp.helper.Logger;
import com.emts.restaurantapp.helper.NetworkUtils;
import com.emts.restaurantapp.helper.PreferenceHelper;
import com.emts.restaurantapp.helper.VolleyHelper;
import com.emts.restaurantapp.models.CategoryModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;


public class HomeFragment extends Fragment {
    ArrayList<Integer> sliderImage = new ArrayList<>(Arrays.asList(R.drawable.slider1, R.drawable.slider2, R.drawable.slider3, R.drawable.slider4));
    ArrayList<CategoryModel> cuisinesList;
    ProgressBar progressBar;
    TextView errText;
    RecyclerView cuisinesListing;
    PreferenceHelper helper;
    CategoryAdapter categoryAdapter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        helper = PreferenceHelper.getInstance(getActivity());

        cuisinesList = new ArrayList<>();
        setBannerImage(view, sliderImage);

        TabLayout tabLayout = view.findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText("Most Selling"));
        tabLayout.addTab(tabLayout.newTab().setText("Menu Today"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        final ViewPager viewPager = view.findViewById(R.id.pager);
        PageAdapter pageAdapter = new PageAdapter(getChildFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(pageAdapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });


        cuisinesListing = view.findViewById(R.id.recycler_cuisines);
        cuisinesListing.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        cuisinesListing.setNestedScrollingEnabled(false);
        categoryAdapter = new CategoryAdapter(cuisinesList, getActivity());
        cuisinesListing.setAdapter(categoryAdapter);

        errText = view.findViewById(R.id.error_text);
        progressBar = view.findViewById(R.id.progress_bar);

        if (NetworkUtils.isInNetwork(getActivity())) {
            userCategoryListingTask();
        } else {
            errText.setText("No Internet Connection...");
            errText.setVisibility(View.VISIBLE);
        }


    }


    private void setBannerImage(View view, final ArrayList<Integer> images) {
        final SliderLayout imageSlider = view.findViewById(R.id.slider);
        final PagerIndicator pagerIndicator = view.findViewById(R.id.custom_indicator);

        imageSlider.stopAutoCycle();
        imageSlider.setVisibility(View.GONE);
        for (Integer name : images) {
            DefaultSliderView defaultSlider = new DefaultSliderView(getActivity());
            // initialize a SliderLayout
            defaultSlider
                    .image(name)
                    .setScaleType(BaseSliderView.ScaleType.Fit);

            //add your extra information
            defaultSlider.bundle(new Bundle());
            defaultSlider.getBundle()
                    .putString("extra", String.valueOf(name));

            imageSlider.addSlider(defaultSlider);

            try {
                Class<?> c = pagerIndicator.getClass();
                Field f = c.getDeclaredField("mPreviousSelectedPosition");
                f.setAccessible(true);
                f.setInt(pagerIndicator, -1);
                f.setAccessible(false);
            } catch (Exception e) {
                Logger.e("reflection ex", e.getMessage() + " ");
            }
        }

        if (images.size() > 1) {
            imageSlider.setCurrentPosition(0); //workaround for auto slide to second
            imageSlider.setDuration(40000);
            imageSlider.setCustomAnimation(new DescriptionAnimation());
            imageSlider.setPresetTransformer(SliderLayout.Transformer.Default);

            try {
                Class<?> c = pagerIndicator.getClass();
                Field f = c.getDeclaredField("mPreviousSelectedPosition");
                f.setAccessible(true);
                f.setInt(pagerIndicator, -1);
                f.setAccessible(false);
            } catch (Exception e) {
                Logger.e("reflection ex", e.getMessage() + " ");
            }
            imageSlider.setCustomIndicator(pagerIndicator);
            imageSlider.setCurrentPosition(0, true);
            // play from first image, when all images loaded
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    try {
                        Class<?> c = pagerIndicator.getClass();
                        Field f = c.getDeclaredField("mPreviousSelectedPosition");
                        f.setAccessible(true);
                        f.setInt(pagerIndicator, -1);
                        f.setAccessible(false);

                        imageSlider.setCustomIndicator(pagerIndicator);
                        pagerIndicator.onPageSelected(0);
                        imageSlider.setCurrentPosition(0);
                        imageSlider.setDuration(4000);
                        imageSlider.setVisibility(View.VISIBLE);

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    imageSlider.startAutoCycle();
                                } catch (Exception e) {
                                }
                            }
                        }, 4400);
                    } catch (Exception e) {
                        Logger.e("reflection ex", e.getMessage() + " ");
                    }
                }
            }, 900);

        } else {
            imageSlider.setVisibility(View.VISIBLE);
            pagerIndicator.setVisibility(View.GONE);
            imageSlider.setIndicatorVisibility(PagerIndicator.IndicatorVisibility.Invisible);
            imageSlider.stopAutoCycle();
        }
    }


    private void userCategoryListingTask() {
        progressBar.setVisibility(View.VISIBLE);
        cuisinesListing.setVisibility(View.GONE);
        errText.setVisibility(View.GONE);
        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postMap = vHelper.getPostParams();


        vHelper.addVolleyRequestListeners(Api.getInstance().categoryUrl, Request.Method.POST, postMap,
                new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                JSONArray category = res.getJSONArray("cuisines_data");
                                for (int i = 0; i < category.length(); i++) {
                                    JSONObject jsonObject = category.getJSONObject(i);
                                    CategoryModel categoryModel = new CategoryModel();
                                    categoryModel.setCuisineName(jsonObject.getString("name"));
                                    categoryModel.setId(jsonObject.getString("id"));
                                    categoryModel.setDesc(jsonObject.getString("short_desc"));
                                    cuisinesList.add(categoryModel);
                                }
                                categoryAdapter.notifyDataSetChanged();
                                cuisinesListing.setVisibility(View.VISIBLE);


                            } else {
                                errText.setVisibility(View.VISIBLE);
                                errText.setText(res.getString("message"));
                            }
                        } catch (JSONException e) {
                            Logger.e("userCategoryListing json ex", e.getMessage() + "");
                        }
                        progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError error) {
                        progressBar.setVisibility(View.GONE);
                        errText.setVisibility(View.VISIBLE);
                        try {
                            JSONObject errorObj = new JSONObject(new String(error.networkResponse.data));
                        } catch (Exception e) {
                            if (error instanceof NetworkError) {
                                Toast.makeText(getActivity(), "No Internet Connection...", Toast.LENGTH_SHORT).show();
                            } else if (error instanceof ServerError) {
                                Toast.makeText(getActivity(), "Server Error...", Toast.LENGTH_SHORT).show();
                            } else if (error instanceof AuthFailureError) {
                                Toast.makeText(getActivity(), "Authentication Error...", Toast.LENGTH_SHORT).show();
                            } else if (error instanceof ParseError) {
                                Toast.makeText(getActivity(), "Parse Error", Toast.LENGTH_SHORT).show();
                            } else if (error instanceof TimeoutError) {
                                Toast.makeText(getActivity(), "Timed Out", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }, "userCategoryListingTask");
    }
}
