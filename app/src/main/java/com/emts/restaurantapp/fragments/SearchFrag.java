package com.emts.restaurantapp.fragments;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.emts.restaurantapp.R;
import com.emts.restaurantapp.adapter.FoodAdapter;
import com.emts.restaurantapp.helper.Api;
import com.emts.restaurantapp.helper.Logger;
import com.emts.restaurantapp.helper.NetworkUtils;
import com.emts.restaurantapp.helper.PreferenceHelper;
import com.emts.restaurantapp.helper.VolleyHelper;
import com.emts.restaurantapp.models.CategoryModel;
import com.emts.restaurantapp.models.FoodMdl;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by User on 2017-07-25.
 */

public class SearchFrag extends Fragment {
    PreferenceHelper helper;
    ImageView searchIcon;
    TextView errText;
    ProgressBar progressBar;
    EditText searchField;

    RecyclerView foodListing;
    ArrayList<FoodMdl> searchList;
    FoodAdapter searchAdapter;

    CategoryModel categoryModel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            categoryModel = (CategoryModel) getArguments().getSerializable("Category");

        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_search, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        helper = PreferenceHelper.getInstance(getActivity());
        searchList = new ArrayList<>();
        foodListing = view.findViewById(R.id.list_food);
        foodListing.setLayoutManager(new LinearLayoutManager(getActivity()));
        foodListing.setNestedScrollingEnabled(false);
        searchAdapter = new FoodAdapter(getActivity(), searchList);
        foodListing.setAdapter(searchAdapter);

        searchIcon = view.findViewById(R.id.search_icon);
        errText = view.findViewById(R.id.error_text);
        progressBar = view.findViewById(R.id.progress_bar);
        searchField = view.findViewById(R.id.search_field);
        searchField.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {

                if (i == EditorInfo.IME_ACTION_SEARCH) {

                    if (NetworkUtils.isInNetwork(getActivity())) {
                        userSearchFoodTask();
                    } else {
                        errText.setText("No Internet Connection...");
                        errText.setVisibility(View.VISIBLE);
                        foodListing.setVisibility(View.GONE);
                        searchIcon.setVisibility(View.GONE);
                    }
                }
                return false;
            }
        });
        if (categoryModel != null) {
            userCategorySearchFoodTask();
        }
    }


    private void userSearchFoodTask() {
        searchList.clear();
        searchAdapter.notifyDataSetChanged();
        progressBar.setVisibility(View.VISIBLE);
        errText.setVisibility(View.GONE);
        foodListing.setVisibility(View.GONE);
        searchIcon.setVisibility(View.GONE);

        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postMap = vHelper.getPostParams();
        postMap.put("search_keyword", searchField.getText().toString().trim());


        vHelper.addVolleyRequestListeners(Api.getInstance().searchFoodUrl, Request.Method.POST, postMap,
                new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                JSONArray searchArray = res.getJSONArray("result");
                                if (searchArray.length() == 0) {
                                    errText.setVisibility(View.VISIBLE);
                                    errText.setText(res.getString("message"));
                                    foodListing.setVisibility(View.GONE);
                                    searchIcon.setVisibility(View.GONE);
                                    progressBar.setVisibility(View.GONE);
                                    return;
                                }
                                for (int i = 0; i < searchArray.length(); i++) {
                                    JSONObject jsonObject = searchArray.getJSONObject(i);
                                    FoodMdl foodMdl = new FoodMdl();
                                    foodMdl.setProductId("id");
                                    foodMdl.setFoodName(jsonObject.getString("name"));
                                    foodMdl.setIngredients(jsonObject.getString("ingredients"));
                                    foodMdl.setFoodPrice(Double.parseDouble(jsonObject.getString("retail_price")));
                                    foodMdl.setFoodImage(jsonObject.getString("image"));
                                    searchList.add(foodMdl);
                                }
                                searchAdapter.notifyDataSetChanged();
                                foodListing.setVisibility(View.VISIBLE);
                                errText.setVisibility(View.GONE);
                                searchIcon.setVisibility(View.GONE);

                            } else {
                                errText.setVisibility(View.VISIBLE);
                                errText.setText(res.getString("message"));
                                foodListing.setVisibility(View.GONE);
                                searchIcon.setVisibility(View.GONE);
                            }
                        } catch (JSONException e) {
                            Logger.e("userSearchFoodTask json ex", e.getMessage() + "");
                        }
                        progressBar.setVisibility(View.GONE);
                        searchIcon.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError error) {
                        progressBar.setVisibility(View.GONE);
                        searchIcon.setVisibility(View.GONE);
                        try {
                            JSONObject errorObj = new JSONObject(new String(error.networkResponse.data));
                        } catch (Exception e) {
                            if (error instanceof NetworkError) {
                                Toast.makeText(getActivity(), "No Internet Connection...", Toast.LENGTH_SHORT).show();
                            } else if (error instanceof ServerError) {
                                Toast.makeText(getActivity(), "Server Error...", Toast.LENGTH_SHORT).show();
                            } else if (error instanceof AuthFailureError) {
                                Toast.makeText(getActivity(), "Authentication Error...", Toast.LENGTH_SHORT).show();
                            } else if (error instanceof ParseError) {
                                Toast.makeText(getActivity(), "Parse Error", Toast.LENGTH_SHORT).show();
                            } else if (error instanceof TimeoutError) {
                                Toast.makeText(getActivity(), "Timed Out", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }, "userSearchFoodTask");
    }

    private void userCategorySearchFoodTask() {
        searchList.clear();
        searchAdapter.notifyDataSetChanged();
        progressBar.setVisibility(View.VISIBLE);
        errText.setVisibility(View.GONE);
        foodListing.setVisibility(View.GONE);
        searchIcon.setVisibility(View.GONE);

        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postMap = vHelper.getPostParams();
        postMap.put("search_keyword", categoryModel.getId());


        vHelper.addVolleyRequestListeners(Api.getInstance().searchByCategoryurl, Request.Method.POST, postMap,
                new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                JSONArray searchArray = res.getJSONArray("result");
                                if (searchArray.length() == 0) {
                                    errText.setVisibility(View.VISIBLE);
                                    errText.setText(res.getString("message"));
                                    foodListing.setVisibility(View.GONE);
                                    searchIcon.setVisibility(View.GONE);
                                    progressBar.setVisibility(View.GONE);
                                    return;
                                }
                                for (int i = 0; i < searchArray.length(); i++) {
                                    JSONObject jsonObject = searchArray.getJSONObject(i);
                                    FoodMdl foodMdl = new FoodMdl();
                                    foodMdl.setProductId("id");
                                    foodMdl.setFoodName(jsonObject.getString("name"));
                                    foodMdl.setIngredients(jsonObject.getString("ingredients"));
                                    foodMdl.setFoodPrice(Double.parseDouble(jsonObject.getString("retail_price")));
                                    foodMdl.setFoodImage(jsonObject.getString("image"));
                                    searchList.add(foodMdl);
                                }
                                searchAdapter.notifyDataSetChanged();
                                foodListing.setVisibility(View.VISIBLE);
                                errText.setVisibility(View.GONE);
                                searchIcon.setVisibility(View.GONE);

                            } else {
                                errText.setVisibility(View.VISIBLE);
                                errText.setText(res.getString("message"));
                                foodListing.setVisibility(View.GONE);
                                searchIcon.setVisibility(View.GONE);
                            }
                        } catch (JSONException e) {
                            Logger.e("userCategorySearchFoodTask json ex", e.getMessage() + "");
                        }
                        progressBar.setVisibility(View.GONE);
                        searchIcon.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError error) {
                        progressBar.setVisibility(View.GONE);
                        searchIcon.setVisibility(View.GONE);
                        try {
                            JSONObject errorObj = new JSONObject(new String(error.networkResponse.data));
                        } catch (Exception e) {
                            if (error instanceof NetworkError) {
                                Toast.makeText(getActivity(), "No Internet Connection...", Toast.LENGTH_SHORT).show();
                            } else if (error instanceof ServerError) {
                                Toast.makeText(getActivity(), "Server Error...", Toast.LENGTH_SHORT).show();
                            } else if (error instanceof AuthFailureError) {
                                Toast.makeText(getActivity(), "Authentication Error...", Toast.LENGTH_SHORT).show();
                            } else if (error instanceof ParseError) {
                                Toast.makeText(getActivity(), "Parse Error", Toast.LENGTH_SHORT).show();
                            } else if (error instanceof TimeoutError) {
                                Toast.makeText(getActivity(), "Timed Out", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }, "userCategorySearchFoodTask");
    }
}


