package com.emts.restaurantapp.fragments;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.emts.restaurantapp.R;
import com.emts.restaurantapp.adapter.VerticalCategoryAdapter;
import com.emts.restaurantapp.helper.Api;
import com.emts.restaurantapp.helper.Logger;
import com.emts.restaurantapp.helper.NetworkUtils;
import com.emts.restaurantapp.helper.PreferenceHelper;
import com.emts.restaurantapp.helper.VolleyHelper;
import com.emts.restaurantapp.models.CategoryModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by User on 2017-07-25.
 */

public class CategoryFrag extends Fragment {
    VerticalCategoryAdapter cuisinesAdapter;
    ArrayList<CategoryModel> categoryList;
    PreferenceHelper helper;
    ProgressBar progressBar;
    TextView errText;
    RecyclerView categoryListing;
    EditText categorySearching;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_cuisine, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        helper = PreferenceHelper.getInstance(getActivity());

        categoryList = new ArrayList<>();
        categoryListing = view.findViewById(R.id.list_cuisines);
        categoryListing.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        categoryListing.setNestedScrollingEnabled(false);
        cuisinesAdapter = new VerticalCategoryAdapter(categoryList, getActivity());
        categoryListing.setAdapter(cuisinesAdapter);

        errText = view.findViewById(R.id.error_text);
        progressBar = view.findViewById(R.id.progress_bar);
        if (NetworkUtils.isInNetwork(getActivity())) {
            userCategoryListingTask();
        } else {
            errText.setText("No Internet Connection...");
            errText.setVisibility(View.VISIBLE);
            categoryListing.setVisibility(View.GONE);
        }

        categorySearching = view.findViewById(R.id.cat_search);
        categorySearching.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int i, int i1, int i2) {


                final String query = s.toString().toLowerCase().trim();
                final ArrayList<CategoryModel> filteredList = new ArrayList<>();

                for (int b = 0; b < categoryList.size(); b++) {

                    final String text = categoryList.get(b).getCuisineName().toLowerCase();
                    if (text.contains(query)) {
                        filteredList.add(categoryList.get(b));
                    }
                }
                categoryListing.setLayoutManager(new LinearLayoutManager(getActivity()));
                cuisinesAdapter = new VerticalCategoryAdapter(filteredList, getActivity());
                categoryListing.setAdapter(cuisinesAdapter);
                cuisinesAdapter.notifyDataSetChanged();

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }


    private void userCategoryListingTask() {
        progressBar.setVisibility(View.VISIBLE);
        errText.setVisibility(View.GONE);
        categoryListing.setVisibility(View.GONE);
        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postMap = vHelper.getPostParams();


        vHelper.addVolleyRequestListeners(Api.getInstance().categoryUrl, Request.Method.POST, postMap,
                new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {

                                JSONArray catArray = res.getJSONArray("cuisines_data");
                                for (int i = 0; i < catArray.length(); i++) {
                                    JSONObject jsonObject = catArray.getJSONObject(i);
                                    CategoryModel categoryModel = new CategoryModel();
                                    categoryModel.setCuisineName(jsonObject.getString("name"));
                                    categoryModel.setId(jsonObject.getString("id"));
                                    categoryModel.setDesc(jsonObject.getString("short_desc"));
                                    categoryList.add(categoryModel);
                                }
                                cuisinesAdapter.notifyDataSetChanged();
                                categoryListing.setVisibility(View.VISIBLE);
                                errText.setVisibility(View.GONE);

                            } else {
                                errText.setVisibility(View.VISIBLE);
                                errText.setText(res.getString("message"));
                                categoryListing.setVisibility(View.GONE);
                            }
                        } catch (JSONException e) {
                            Logger.e("userCategoryListingTask json ex", e.getMessage() + "");
                        }
                        progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError error) {
                        progressBar.setVisibility(View.GONE);
                        try {
                            JSONObject errorObj = new JSONObject(new String(error.networkResponse.data));
                        } catch (Exception e) {
                            if (error instanceof NetworkError) {
                                Toast.makeText(getActivity(), "No Internet Connection...", Toast.LENGTH_SHORT).show();
                            } else if (error instanceof ServerError) {
                                Toast.makeText(getActivity(), "Server Error...", Toast.LENGTH_SHORT).show();
                            } else if (error instanceof AuthFailureError) {
                                Toast.makeText(getActivity(), "Authentication Error...", Toast.LENGTH_SHORT).show();
                            } else if (error instanceof ParseError) {
                                Toast.makeText(getActivity(), "Parse Error", Toast.LENGTH_SHORT).show();
                            } else if (error instanceof TimeoutError) {
                                Toast.makeText(getActivity(), "Timed Out", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }, "userCategoryListingTask");
    }

}


