package com.emts.restaurantapp.fragments;


import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.emts.restaurantapp.R;
import com.emts.restaurantapp.adapter.OrderHistoryAdapter;
import com.emts.restaurantapp.helper.Api;
import com.emts.restaurantapp.helper.Logger;
import com.emts.restaurantapp.helper.NetworkUtils;
import com.emts.restaurantapp.helper.PreferenceHelper;
import com.emts.restaurantapp.helper.VolleyHelper;
import com.emts.restaurantapp.models.OrderHistoryModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class OrderHistoryFrag extends Fragment {
    PreferenceHelper helper;
    ArrayList<OrderHistoryModel> orderList;
    RecyclerView orderListing;
    OrderHistoryAdapter orderHistoryAdapter;
    ProgressBar progressBar;
    TextView errText;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_order_history, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        helper = PreferenceHelper.getInstance(getActivity());

        orderListing = view.findViewById(R.id.order_history_list);
        orderListing.setLayoutManager(new LinearLayoutManager(getActivity()));
        orderListing.setNestedScrollingEnabled(false);
        orderList = new ArrayList<>();
        orderHistoryAdapter = new OrderHistoryAdapter(getActivity(), orderList);
        orderListing.setAdapter(orderHistoryAdapter);

        errText = view.findViewById(R.id.error_text);
        progressBar = view.findViewById(R.id.progress_bar);


        if (NetworkUtils.isInNetwork(getActivity())) {
            userOrderHistoryTask();
        } else {
            errText.setText("No Internet Connection...");
            errText.setVisibility(View.VISIBLE);
            orderListing.setVisibility(View.GONE);
        }
    }

    private void userOrderHistoryTask() {

        progressBar.setVisibility(View.VISIBLE);
        orderListing.setVisibility(View.GONE);
        errText.setVisibility(View.GONE);

        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postMap = vHelper.getPostParams();
        postMap.put("user_id", helper.getUserId());


        vHelper.addVolleyRequestListeners(Api.getInstance().orderHistoryUrl, Request.Method.POST, postMap,
                new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                JSONArray orderHistoryArray = res.getJSONArray("order_history");
                                if (orderHistoryArray.length() == 0) {
                                    orderHistoryMessage();
                                    return;
                                }
                                for (int i = 0; i < orderHistoryArray.length(); i++) {
                                    JSONObject jsonObject = orderHistoryArray.getJSONObject(i);
                                    OrderHistoryModel orderHistoryModel = new OrderHistoryModel();
                                    orderHistoryModel.setDateTime(jsonObject.getString("order_datetime"));
                                    orderHistoryModel.setStatus(jsonObject.getString("delivery_status"));
                                    orderHistoryModel.setInvoice_id(jsonObject.getString("invoice_id"));
                                    orderHistoryModel.setCity(jsonObject.getString("delivery_city"));
                                    orderHistoryModel.setAddress(jsonObject.getString("delivery_address"));
                                    orderHistoryModel.setTotalPrice(Double.parseDouble(jsonObject.getString("total_cost")));
                                    orderList.add(orderHistoryModel);
                                }
                                orderHistoryAdapter.notifyDataSetChanged();
                                orderListing.setVisibility(View.VISIBLE);
                                errText.setVisibility(View.GONE);

                            } else {
                                errText.setVisibility(View.VISIBLE);
                                errText.setText(res.getString("message"));
                                orderListing.setVisibility(View.GONE);
                            }
                        } catch (JSONException e) {
                            Logger.e("userOrderHistoryTask json ex", e.getMessage() + "");
                        }
                        progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError error) {
                        progressBar.setVisibility(View.GONE);
                        try {
                            JSONObject errorObj = new JSONObject(new String(error.networkResponse.data));
                        } catch (Exception e) {
                            if (error instanceof NetworkError) {
                                Toast.makeText(getActivity(), "No Internet Connection...", Toast.LENGTH_SHORT).show();
                            } else if (error instanceof ServerError) {
                                Toast.makeText(getActivity(), "Server Error...", Toast.LENGTH_SHORT).show();
                            } else if (error instanceof AuthFailureError) {
                                Toast.makeText(getActivity(), "Authentication Error...", Toast.LENGTH_SHORT).show();
                            } else if (error instanceof ParseError) {
                                Toast.makeText(getActivity(), "Parse Error", Toast.LENGTH_SHORT).show();
                            } else if (error instanceof TimeoutError) {
                                Toast.makeText(getActivity(), "Timed Out", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }, "userOrderHistoryTask");
    }

    public void orderHistoryMessage() {
        errText.setText("No Order History...");
        orderListing.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);
        errText.setVisibility(View.VISIBLE);
    }
}
