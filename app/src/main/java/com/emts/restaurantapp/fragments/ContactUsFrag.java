package com.emts.restaurantapp.fragments;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.dd.CircularProgressButton;
import com.emts.restaurantapp.R;
import com.emts.restaurantapp.helper.AlertUtils;
import com.emts.restaurantapp.helper.Api;
import com.emts.restaurantapp.helper.Logger;
import com.emts.restaurantapp.helper.NetworkUtils;
import com.emts.restaurantapp.helper.PreferenceHelper;
import com.emts.restaurantapp.helper.VolleyHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class ContactUsFrag extends Fragment {
    PreferenceHelper helper;
    EditText eMail, contactNumber, enterMessage;
    TextView errEmail, errContact, errMessage;
    CircularProgressButton contactUs;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_contact_us, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        helper = PreferenceHelper.getInstance(getActivity());
        eMail = view.findViewById(R.id.edt_email);
        contactNumber = view.findViewById(R.id.edt_contact_number);
        enterMessage = view.findViewById(R.id.edt_enter_message);

        errEmail = view.findViewById(R.id.err_email);
        errContact = view.findViewById(R.id.err_contact);
        errMessage = view.findViewById(R.id.err_message);

        contactUs = view.findViewById(R.id.btn_contact_us);
        contactUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (NetworkUtils.isInNetwork(getActivity())) {

                    if (validation()) {
//                    signIn.setProgress(50);

                        if (NetworkUtils.isInNetwork(getActivity())) {
                            userContactUsTask();

                        } else {
                            AlertUtils.showSnack(getActivity(), view, "No internet connection...");
                        }
                        // turn on indeterminate progress
                        // set progress > 0 & < 100 to display indeterminate progress
//                signIn.setProgress(100); // set progress to 100 or -1 to indicate complete or error state
//                signIn.setProgress(0); // set progress to 0 to switch back to normal state
                    } else {

                    }
                }
            }
        });

    }

    private void userContactUsTask() {
        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postMap = vHelper.getPostParams();
        postMap.put("email", eMail.getText().toString().trim());
        postMap.put("message", enterMessage.getText().toString().trim());

        vHelper.addVolleyRequestListeners(Api.getInstance().contactUsUrl, Request.Method.POST, postMap,
                new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                AlertUtils.showSnack(getActivity(), eMail, res.getString("display"));

                            } else {
                            }
                        } catch (JSONException e) {
                            Logger.e("userContactUsTask json ex", e.getMessage() + "");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError error) {
                        try {
                            JSONObject errorObj = new JSONObject(new String(error.networkResponse.data));

                        } catch (Exception e) {
                            if (error instanceof NetworkError) {
                                Toast.makeText(getActivity(), "No Internet Connection...", Toast.LENGTH_SHORT).show();
                            } else if (error instanceof ServerError) {
                                Toast.makeText(getActivity(), "Server Error...", Toast.LENGTH_SHORT).show();
                            } else if (error instanceof AuthFailureError) {
                                Toast.makeText(getActivity(), "Authentication Error...", Toast.LENGTH_SHORT).show();
                            } else if (error instanceof ParseError) {
                                Toast.makeText(getActivity(), "Parse Error", Toast.LENGTH_SHORT).show();
                            } else if (error instanceof TimeoutError) {
                                Toast.makeText(getActivity(), "Timed Out", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }, "userContactUsTask");
    }

    public boolean validation() {
        boolean isValid = true;
        if (TextUtils.isEmpty(eMail.getText().toString().trim())) {
            isValid = false;
            errEmail.setVisibility(View.VISIBLE);
            errEmail.setText("This Field is required...");
        } else {
            if (Patterns.EMAIL_ADDRESS.matcher(eMail.getText().toString().trim()).matches()) {
                errEmail.setVisibility(View.GONE);
            } else {
                Toast.makeText(getActivity(), "Invalid Email Address", Toast.LENGTH_SHORT).show();
            }
        }

        if (TextUtils.isEmpty(contactNumber.getText().toString().trim())) {
            isValid = false;
            errContact.setVisibility(View.VISIBLE);
            errContact.setText("This field is required...");
        } else {
            errContact.setVisibility(View.GONE);

        }

        if (TextUtils.isEmpty(enterMessage.getText().toString().trim())) {
            isValid = false;
            errMessage.setVisibility(View.VISIBLE);
            errMessage.setText("This field is required...");
        } else {
            errMessage.setVisibility(View.GONE);

        }

        return isValid;
    }
}
