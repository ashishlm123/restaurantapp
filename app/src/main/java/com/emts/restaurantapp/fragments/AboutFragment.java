package com.emts.restaurantapp.fragments;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.core.widget.NestedScrollView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.emts.restaurantapp.R;
import com.emts.restaurantapp.helper.Api;
import com.emts.restaurantapp.helper.Logger;
import com.emts.restaurantapp.helper.NetworkUtils;
import com.emts.restaurantapp.helper.PreferenceHelper;
import com.emts.restaurantapp.helper.VolleyHelper;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by User on 2017-07-25.
 */

public class AboutFragment extends Fragment {
    PreferenceHelper helper;
    ImageView resLogo;
    TextView resName, resLoc, resCon, resStatus, resCat, resHours, resDeliveryTime,
            resMinOrder, resDeliveryFee, resPreOrder, paymentOptions;
    ProgressBar progressBar;
    TextView errText;
    NestedScrollView aboutPage;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_about, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        helper = PreferenceHelper.getInstance(getActivity());

        aboutPage = view.findViewById(R.id.about_page);

        resLogo = view.findViewById(R.id.res_logo);
        resName = view.findViewById(R.id.res_name);
        resLoc = view.findViewById(R.id.res_loc);
        resCon = view.findViewById(R.id.res_contact);
        resStatus = view.findViewById(R.id.res_status);
        resCat = view.findViewById(R.id.res_cat);
        resHours = view.findViewById(R.id.res_hours);
        resDeliveryTime = view.findViewById(R.id.res_delivery_time);
        resMinOrder = view.findViewById(R.id.res_min_order);
        resDeliveryFee = view.findViewById(R.id.res_delivery_fee);
        resPreOrder = view.findViewById(R.id.res_pre_order);
        paymentOptions = view.findViewById(R.id.payment_options);


        errText = view.findViewById(R.id.error_text);
        progressBar = view.findViewById(R.id.progress_bar);
        if (NetworkUtils.isInNetwork(getActivity())) {
            userAboutTask();
        } else {
            errText.setText("No Internet Connection...");
            errText.setVisibility(View.VISIBLE);
            aboutPage.setVisibility(View.GONE);
        }
    }

    private void userAboutTask() {
        progressBar.setVisibility(View.VISIBLE);
        errText.setVisibility(View.GONE);
        aboutPage.setVisibility(View.GONE);
        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postMap = vHelper.getPostParams();


        vHelper.addVolleyRequestListeners(Api.getInstance().aboutUrl, Request.Method.POST, postMap,
                new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                JSONArray about = res.getJSONArray("about_us");
                                JSONObject jsonObject = about.getJSONObject(0);
                                String imagePath = res.getString("image_path") + jsonObject.getString("site_logo");
                                Picasso.with(getActivity()).load(imagePath).into(resLogo);
                                resName.setText(jsonObject.getString("site_name"));
                                resLoc.setText(jsonObject.getString("location"));
                                resCon.setText(jsonObject.getString("contact_number"));
                                resStatus.setText("Open");
                                resCat.setText(jsonObject.getString("cuisines"));
                                resHours.setText(jsonObject.getString("opening_hours"));
                                resDeliveryFee.setText("USD 0.5 To USD 20.05");
                                resDeliveryTime.setText(jsonObject.getString("delivery_time"));
                                resMinOrder.setText(jsonObject.getString("minimum_order"));
                                resPreOrder.setText(jsonObject.getString("pre_order"));
                                paymentOptions.setText(jsonObject.getString("payment_option"));

                                aboutPage.setVisibility(View.VISIBLE);
                                errText.setVisibility(View.GONE);
                            } else {
                                errText.setVisibility(View.VISIBLE);
                                errText.setText(res.getString("message"));
                                aboutPage.setVisibility(View.GONE);
                            }


                        } catch (JSONException e) {
                            Logger.e("userAboutTask json ex", e.getMessage() + "");

                        }
                        progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError error) {
                        try {
                            JSONObject errorObj = new JSONObject(new String(error.networkResponse.data));
                        } catch (Exception e) {
                            if (error instanceof NetworkError) {
                                Toast.makeText(getActivity(), "No Internet Connection...", Toast.LENGTH_SHORT).show();
                            } else if (error instanceof ServerError) {
                                Toast.makeText(getActivity(), "Server Error...", Toast.LENGTH_SHORT).show();
                            } else if (error instanceof AuthFailureError) {
                                Toast.makeText(getActivity(), "Authentication Error...", Toast.LENGTH_SHORT).show();
                            } else if (error instanceof ParseError) {
                                Toast.makeText(getActivity(), "Parse Error", Toast.LENGTH_SHORT).show();
                            } else if (error instanceof TimeoutError) {
                                Toast.makeText(getActivity(), "Timed Out", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }, "userAboutTask");
    }
}

