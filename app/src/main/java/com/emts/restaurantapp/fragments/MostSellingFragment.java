package com.emts.restaurantapp.fragments;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.emts.restaurantapp.R;
import com.emts.restaurantapp.adapter.FoodAdapter;
import com.emts.restaurantapp.helper.Api;
import com.emts.restaurantapp.helper.Logger;
import com.emts.restaurantapp.helper.NetworkUtils;
import com.emts.restaurantapp.helper.PreferenceHelper;
import com.emts.restaurantapp.helper.VolleyHelper;
import com.emts.restaurantapp.models.FoodMdl;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Prabin on 8/3/2017.
 */

public class MostSellingFragment extends Fragment {

    ArrayList<FoodMdl> foodList;
    PreferenceHelper helper;
    RecyclerView mostSellingList;
    ProgressBar progressBar;
    TextView errText;
    FoodAdapter mostSellingAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_list, container, false);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        helper = PreferenceHelper.getInstance(getActivity());

        mostSellingList = view.findViewById(R.id.list_recycler);
        mostSellingList.setLayoutManager(new LinearLayoutManager(getActivity()));
        mostSellingList.setNestedScrollingEnabled(false);
        foodList = new ArrayList<>();
        mostSellingAdapter = new FoodAdapter(getActivity(), foodList);
        mostSellingList.setAdapter(mostSellingAdapter);

        errText = view.findViewById(R.id.error_text);
        progressBar = view.findViewById(R.id.progress_bar);

        if (NetworkUtils.isInNetwork(getActivity())) {
            userMostSellingListingTask();
        } else {
            errText.setText("No Internet Connection...");
            errText.setVisibility(View.VISIBLE);
            mostSellingList.setVisibility(View.GONE);
        }

    }

    private void userMostSellingListingTask() {
        progressBar.setVisibility(View.VISIBLE);
        mostSellingList.setVisibility(View.GONE);
        errText.setVisibility(View.GONE);

        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postMap = vHelper.getPostParams();

        vHelper.addVolleyRequestListeners(Api.getInstance().mostSellingUrl, Request.Method.POST, postMap,
                new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {

                                JSONArray mostSellingArray = res.getJSONArray("ms_data");
                                for (int i = 0; i < mostSellingArray.length(); i++) {
                                    JSONObject jsonObject = mostSellingArray.getJSONObject(i);
                                    FoodMdl foodMdl = new FoodMdl();
                                    foodMdl.setProductId(jsonObject.getString("product_id"));
                                    foodMdl.setFoodName(jsonObject.getString("name"));
                                    foodMdl.setFoodImage(jsonObject.getString("image"));
                                    foodMdl.setIngredients(jsonObject.getString("ingredients"));
                                    foodMdl.setFoodPrice(Double.parseDouble(jsonObject.getString("price")));
                                    foodList.add(foodMdl);
                                }

                                mostSellingAdapter.notifyDataSetChanged();
                                mostSellingList.setVisibility(View.VISIBLE);
                                errText.setVisibility(View.GONE);


                            } else {
                                errText.setVisibility(View.VISIBLE);
                                errText.setText(res.getString("message"));
                                mostSellingList.setVisibility(View.GONE);
                            }


                        } catch (JSONException e) {
                            Logger.e("userMenuTodayListingTask json ex", e.getMessage() + "");
                        }
                        progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError error) {

                        progressBar.setVisibility(View.GONE);
                        try {
                            JSONObject errorObj = new JSONObject(new String(error.networkResponse.data));
                        } catch (Exception e) {
                            if (error instanceof NetworkError) {
                                Toast.makeText(getActivity(), "No Internet Connection...", Toast.LENGTH_SHORT).show();
                            } else if (error instanceof ServerError) {
                                Toast.makeText(getActivity(), "Server Error...", Toast.LENGTH_SHORT).show();
                            } else if (error instanceof AuthFailureError) {
                                Toast.makeText(getActivity(), "Authentication Error...", Toast.LENGTH_SHORT).show();
                            } else if (error instanceof ParseError) {
                                Toast.makeText(getActivity(), "Parse Error", Toast.LENGTH_SHORT).show();
                            } else if (error instanceof TimeoutError) {
                                Toast.makeText(getActivity(), "Timed Out", Toast.LENGTH_SHORT).show();
                            }
                        }

                    }
                }, "userMostSellingListingTask");

    }
}
